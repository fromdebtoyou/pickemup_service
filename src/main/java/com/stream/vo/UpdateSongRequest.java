package com.stream.vo;

import java.util.List;

public class UpdateSongRequest {
	
	public UpdateSongRequest(){}
	
	private List<SongVO> updateSongsList;
	
	private int membership_id;
	
	private boolean isValid;
	
	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public int getMembership_id() {
		return membership_id;
	}

	public void setMembership_id(int membership_id) {
		this.membership_id = membership_id;
	}

	public List<SongVO> getUpdateSongsList() {
		return updateSongsList;
	}

	public void setUpdateSongsList(List<SongVO> updateSongsList) {
		this.updateSongsList = updateSongsList;
	}
	
}
