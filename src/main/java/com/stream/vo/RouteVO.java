package com.stream.vo;

import java.util.Date;
import java.util.List;

import com.stream.vo.PickUpPointsVO;

public class RouteVO {
	
	private int routeid;
	private String origin;
	private String destination;
	private String starttime;
	private String fare;
	private int capacity;
	private int isActive;
	private int providedby;
	private Date createdOn;
	private String username;
	
	private PickUpPointsVO[] pickupPoints;
	
	public RouteVO(){}
		
	public RouteVO(String origin, String destination, int providedby, String starttime,
			String fare, int capacity, int isActive) {
		super();
		this.origin = origin;
		this.destination = destination;
		this.providedby = providedby;
		this.starttime = starttime;
		this.fare = fare;
		this.capacity = capacity;
		this.isActive = isActive;
	}
	
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getFare() {
		return fare;
	}
	public void setFare(String fare) {
		this.fare = fare;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public int getProvidedby() {
		return providedby;
	}

	public void setProvidedby(int providedby) {
		this.providedby = providedby;
	}

	public int getRouteid() {
		return routeid;
	}

	public void setRouteid(int routeid) {
		this.routeid = routeid;
	}

	public PickUpPointsVO[] getPickupPoints() {
		return pickupPoints;
	}

	public void setPickupPoints(PickUpPointsVO[] pickupPoints) {
		this.pickupPoints = pickupPoints;
	}
	
}
