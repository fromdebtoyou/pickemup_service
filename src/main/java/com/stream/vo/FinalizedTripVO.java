package com.stream.vo;

public class FinalizedTripVO {
	
	private int tripId;
	private int routeId;
	private PassengerVO[] subscriberArray;

	public int getTripId() {
		return tripId;
	}
	public void setTripId(int tripId) {
		this.tripId = tripId;
	}
	public int getRouteId() {
		return routeId;
	}
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}
	public PassengerVO[] getSubscriberArray() {
		return subscriberArray;
	}
	public void setSubscriberArray(PassengerVO[] subscriberArray) {
		this.subscriberArray = subscriberArray;
	}
	
}
