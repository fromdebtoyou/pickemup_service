package com.stream.vo;

public class SongVO {
	
	private SongMetadata sngMtdt;
	private String updateStatus;//A for 'Active' - D for 'To Be Deleted'
	
	public SongVO(){}
	
	public String getUpdateStatus() {
		return updateStatus;
	}

	public void setUpdateStatus(String updateStatus) {
		this.updateStatus = updateStatus;
	}

	public SongMetadata getSngMtdt() {
		return sngMtdt;
	}
	public void setSngMtdt(SongMetadata sngMtdt) {
		this.sngMtdt = sngMtdt;
	}

}
