package com.stream.vo;

import java.util.List;

public class SongPlayListAssociationVO {
	
	private PlayList objPlayList;
	private List<SongVO> objSongs;
	
	public SongPlayListAssociationVO(){
		
	}

	public PlayList getObjPlayList() {
		return objPlayList;
	}

	public void setObjPlayList(PlayList objPlayList) {
		this.objPlayList = objPlayList;
	}

	public List<SongVO> getObjSongs() {
		return objSongs;
	}

	public void setObjSongs(List<SongVO> objSongs) {
		this.objSongs = objSongs;
	}

	
}
