package com.stream.vo;

public class PlayListDetails {
	
	private int id;
	private int jam_id;
	private int song;
	
	public PlayListDetails(){
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getJam_id() {
		return jam_id;
	}
	public void setJam_id(int jam_id) {
		this.jam_id = jam_id;
	}
	public int getSong() {
		return song;
	}
	public void setSong(int song) {
		this.song = song;
	}
	
}
