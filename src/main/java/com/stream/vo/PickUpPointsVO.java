package com.stream.vo;

public class PickUpPointsVO {
	
	private int pickuppointid;
	private int routeid;
	private String pickupPoint;
	private int isActive;
	private int isOrigin;
	private int isDestination;
	private double latitude;
	private double longitude;
	
	public PickUpPointsVO() {
		
	}
	public PickUpPointsVO(int pickuppointid, int routeid, String pickupPoint,
			int isActive) {
		super();
		this.pickuppointid = pickuppointid;
		this.routeid = routeid;
		this.pickupPoint = pickupPoint;
		this.isActive = isActive;
	}
	
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getIsOrigin() {
		return isOrigin;
	}
	public void setIsOrigin(int isOrigin) {
		this.isOrigin = isOrigin;
	}
	public int getIsDestination() {
		return isDestination;
	}
	public void setIsDestination(int isDestination) {
		this.isDestination = isDestination;
	}
	public int getPickuppointid() {
		return pickuppointid;
	}
	public void setPickuppointid(int pickuppointid) {
		this.pickuppointid = pickuppointid;
	}
	public int getRouteid() {
		return routeid;
	}
	public void setRouteid(int routeid) {
		this.routeid = routeid;
	}
	public String getPickupPoint() {
		return pickupPoint;
	}
	public void setPickupPoint(String pickupPoint) {
		this.pickupPoint = pickupPoint;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	
	

}
