package com.stream.vo;

public class SongMetadata {
	
	private int id;
	private int track_number;
	private String song_name;
	private String song_description;
	private String album_name;
	private String album_description;
	private String album_image;
	private String artist_name;
	private String artist_description;
	private String artist_image;
	private String genre;
	private String file_location;
	private String stream_url;

	public SongMetadata(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public int getTrack_number() {
		return track_number;
	}

	public void setTrack_number(int track_number) {
		this.track_number = track_number;
	}

	public String getSong_name() {
		return song_name;
	}

	public void setSong_name(String song_name) {
		this.song_name = song_name;
	}

	public String getSong_description() {
		return song_description;
	}

	public void setSong_description(String song_description) {
		this.song_description = song_description;
	}

	public String getAlbum_name() {
		return album_name;
	}

	public void setAlbum_name(String album_name) {
		this.album_name = album_name;
	}

	public String getAlbum_description() {
		return album_description;
	}

	public void setAlbum_description(String album_description) {
		this.album_description = album_description;
	}

	public String getAlbum_image() {
		return album_image;
	}

	public void setAlbum_image(String album_image) {
		this.album_image = album_image;
	}

	public String getArtist_name() {
		return artist_name;
	}

	public void setArtist_name(String artist_name) {
		this.artist_name = artist_name;
	}

	public String getArtist_description() {
		return artist_description;
	}

	public void setArtist_description(String artist_description) {
		this.artist_description = artist_description;
	}

	public String getArtist_image() {
		return artist_image;
	}

	public void setArtist_image(String artist_image) {
		this.artist_image = artist_image;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getFile_location() {
		return file_location;
	}

	public void setFile_location(String file_location) {
		this.file_location = file_location;
	}

	public String getStream_url() {
		return stream_url;
	}

	public void setStream_url(String stream_url) {
		this.stream_url = stream_url;
	}
	
	
}
