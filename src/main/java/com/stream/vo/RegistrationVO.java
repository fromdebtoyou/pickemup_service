package com.stream.vo;

public class RegistrationVO {
	
	private String email;
	private String name;
	private boolean self_registered;
	private int referrer_id;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isSelf_registered() {
		return self_registered;
	}
	public void setSelf_registered(boolean self_registered) {
		this.self_registered = self_registered;
	}
	public int getReferrer_id() {
		return referrer_id;
	}
	public void setReferrer_id(int referrer_id) {
		this.referrer_id = referrer_id;
	}
	
	

}
