package com.stream.vo;

public class UserVO {

	private int userid;
	
	private String username;
	private String mobilenumber;
	private String password;
	private String deviceid;
	private String loginstatus;
	private String imageurl;
	private String govtidtype;
	private String govtidnumber;
	private String pickuppoint;
	
	public String getPickuppoint() {
		return pickuppoint;
	}
	public void setPickuppoint(String pickuppoint) {
		this.pickuppoint = pickuppoint;
	}
	public String getLoginstatus() {
		return loginstatus;
	}
	public void setLoginstatus(String loginstatus) {
		this.loginstatus = loginstatus;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDeviceid() {
		return deviceid;
	}
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}
	
	public String getImageurl() {
		return imageurl;
	}
	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
	public String getGovtidtype() {
		return govtidtype;
	}
	public void setGovtidtype(String govtidtype) {
		this.govtidtype = govtidtype;
	}
	public String getGovtidnumber() {
		return govtidnumber;
	}
	public void setGovtidnumber(String govtidnumber) {
		this.govtidnumber = govtidnumber;
	}
	
}
