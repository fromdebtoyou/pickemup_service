package com.stream.vo;

public class DensityVO {
	
	private int routeid;
	private int startseq;
	private int endseq;
	
	public DensityVO(){}

	public int getRouteid() {
		return routeid;
	}

	public void setRouteid(int routeid) {
		this.routeid = routeid;
	}

	public int getStartseq() {
		return startseq;
	}

	public void setStartseq(int startseq) {
		this.startseq = startseq;
	}

	public int getEndseq() {
		return endseq;
	}

	public void setEndseq(int endseq) {
		this.endseq = endseq;
	}
	
	

}
