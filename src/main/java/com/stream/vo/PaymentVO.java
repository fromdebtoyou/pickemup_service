package com.stream.vo;

import java.sql.Date;
import java.sql.Timestamp;

public class PaymentVO {
	
	private int membership_no;
	private int amount;
	private int plan_id;
	private int validity_month;
	private Date paymentDate;
	private Date validityExpiry;
	private String paymentMode;
	private String refer_id;
	private Timestamp created_time;
	private String auto_renewal;
	private Date next_renewal;
	private int id;
	
	public PaymentVO(){}
	
	public String getAuto_renewal() {
		return auto_renewal;
	}

	public void setAuto_renewal(String auto_renewal) {
		this.auto_renewal = auto_renewal;
	}

	public Date getNext_renewal() {
		return next_renewal;
	}

	public void setNext_renewal(Date next_renewal) {
		this.next_renewal = next_renewal;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getValidityExpiry() {
		return validityExpiry;
	}

	public void setValidityExpiry(Date validityExpiry) {
		this.validityExpiry = validityExpiry;
	}

	public String getRefer_id() {
		return refer_id;
	}

	public void setRefer_id(String refer_id) {
		this.refer_id = refer_id;
	}

	public Timestamp getCreated_time() {
		return created_time;
	}

	public void setCreated_time(Timestamp created_time) {
		this.created_time = created_time;
	}

	public int getMembership_no() {
		return membership_no;
	}
	public void setMembership_no(int membership_no) {
		this.membership_no = membership_no;
	}
	
	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getPlan_id() {
		return plan_id;
	}
	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}
	public int getValidity_month() {
		return validity_month;
	}
	public void setValidity_month(int validity_month) {
		this.validity_month = validity_month;
	}
	
	
}
