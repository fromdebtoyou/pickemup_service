package com.stream.vo;

public class SubscriberVO {

	private int userid;
	private int selectionid;
	private int routeid;
	private int isactive;
	private String username;
	private String imageurl;
	private String govtidtype;
	private String govtidnumber;
	private String pickuppoint;
	
	public int getSelectionid() {
		return selectionid;
	}
	public void setSelectionid(int selectionid) {
		this.selectionid = selectionid;
	}
	public int getRouteid() {
		return routeid;
	}
	public void setRouteid(int routeid) {
		this.routeid = routeid;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public String getPickuppoint() {
		return pickuppoint;
	}
	public void setPickuppoint(String pickuppoint) {
		this.pickuppoint = pickuppoint;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getImageurl() {
		return imageurl;
	}
	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
	public String getGovtidtype() {
		return govtidtype;
	}
	public void setGovtidtype(String govtidtype) {
		this.govtidtype = govtidtype;
	}
	public String getGovtidnumber() {
		return govtidnumber;
	}
	public void setGovtidnumber(String govtidnumber) {
		this.govtidnumber = govtidnumber;
	}
	
}
