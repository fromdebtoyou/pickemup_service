package com.stream.vo;

import java.io.Serializable;

public class KeyValueVO  implements Serializable, Comparable<KeyValueVO> {
	
	/** * SERIAL VERSION UID  */
	private static final long serialVersionUID = 7211951475413442122L;
	
	String key;
	String value;
	
	@SuppressWarnings("unused")
	private KeyValueVO() {
	}
	
	public KeyValueVO(String key, String value) {
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(key == null || ((KeyValueVO)obj).getKey() == null) {
			return false;
		}
		return key.equals(((KeyValueVO)obj).getKey());
	}
	
	@Override
	public int hashCode() {
		if(key != null && key.length() > 0) {
			try {
				return Integer.valueOf(key);
			} catch (Exception e) {
				return 0;
			}
		} else {
			return 0;
		}
	}
	
	public int compareTo(KeyValueVO obj) {
		if(value == null || obj.getValue() == null) {
			return 0;
		}
		return value.compareTo(obj.getValue());
	}
	
	@Override
	public String toString() {
		return value;
	}
	
	public String toJSONString() {
		return "{\"key\":" + key + ", \"value\": " + value + "}";
	}
}
