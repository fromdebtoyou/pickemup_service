package com.stream.vo;

public class RouteRequest {
	private int userId;
	private int startPointId;
	private int endPointId;
	private String journeyTime;
	
	
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getStartPointId() {
		return startPointId;
	}
	public void setStartPointId(int startPointId) {
		this.startPointId = startPointId;
	}
	public int getEndPointId() {
		return endPointId;
	}
	public void setEndPointId(int endPointId) {
		this.endPointId = endPointId;
	}
	public String getJourneyTime() {
		return journeyTime;
	}
	public void setJourneyTime(String journeyTime) {
		this.journeyTime = journeyTime;
	}
	
	
	

}
