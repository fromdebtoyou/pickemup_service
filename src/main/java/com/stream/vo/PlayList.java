package com.stream.vo;

import java.sql.Date;

public class PlayList {

	private int id;
	private int jam_id;
	private Date jam_creation_date;
	private Date jam_validitiy_till;
	private String jam_description;
	
	public PlayList(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getJam_id() {
		return jam_id;
	}

	public void setJam_id(int jam_id) {
		this.jam_id = jam_id;
	}

	public Date getJam_creation_date() {
		return jam_creation_date;
	}

	public void setJam_creation_date(Date jam_creation_date) {
		this.jam_creation_date = jam_creation_date;
	}

	public Date getJam_validitiy_till() {
		return jam_validitiy_till;
	}

	public void setJam_validitiy_till(Date jam_validitiy_till) {
		this.jam_validitiy_till = jam_validitiy_till;
	}

	public String getJam_description() {
		return jam_description;
	}

	public void setJam_description(String jam_description) {
		this.jam_description = jam_description;
	}
	
	
	
}
