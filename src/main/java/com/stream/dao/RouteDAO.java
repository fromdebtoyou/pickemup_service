package com.stream.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.processing.RoundEnvironment;

import org.json.JSONObject;

import com.stream.common.Utility;
import com.stream.db.DB;
import com.stream.vo.PickUpPointsVO;
import com.stream.vo.RouteVO;
import com.stream.vo.SubscriberVO;
import com.stream.vo.TripVO;
import com.stream.vo.UserVO;

public class RouteDAO implements IRouteDAO{

	public RouteVO createRoute(RouteVO routeVO) {
		Connection con = null;
		String query = "insert into routesprovided (providedby,origin,destination,starttime, isactive, createdon,capacity)"+
				"	values (?,?,?,?,?,NOW(),?)" ;
		PreparedStatement stmt = null ;
		int last_inserted_id = 0;
		try{
			con = DB.getConnection();
			stmt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, routeVO.getProvidedby());
			stmt.setString(2, routeVO.getOrigin());
			stmt.setString(3, routeVO.getDestination());
			stmt.setString(4, routeVO.getStarttime());
			stmt.setInt(5, routeVO.getIsActive());
			stmt.setInt(6, routeVO.getCapacity());
			
			stmt.executeUpdate();
			
			ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next())
            {
                last_inserted_id = rs.getInt(1);
            }
			
			if(last_inserted_id == 0){
				routeVO = null;
			}else{
				routeVO.setRouteid(last_inserted_id);
			}
				
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return routeVO;
	}

	public void createPickupPoints(PickUpPointsVO[] pickupPoints,int routeid,String origin,String destination) {
		Connection con = null;
		
		Statement stmt = null ;
		try{
			con = DB.getConnection();
			stmt = con.createStatement();
			String query = "";
			/*String query = "insert into pickuppoints (routeid,pickuppoint,isactive,createdon,isorigin,isdestination,latitude,longitude)"+
					"	values ("+ routeid +",'"+ origin +"',"+1+",NOW(),1,0"+points.getPickupPoint()+")" ;
			stmt.addBatch(query);*/
			int i=0;
			for(PickUpPointsVO points: pickupPoints){
				if(i==0){
					query = "insert into pickuppoints (routeid,pickuppoint,isactive,createdon,isorigin,isdestination,latitude,longitude)"+
						"	values ("+ routeid +",'"+ points.getPickupPoint().split("###")[0] +"',"+1+",NOW(),1,0,"+points.getPickupPoint().split("###")[1]+","+ points.getPickupPoint().split("###")[2]+")" ;
				}else if(i==(pickupPoints.length-1)){
					query = "insert into pickuppoints (routeid,pickuppoint,isactive,createdon,isorigin,isdestination,latitude,longitude)"+
						"	values ("+ routeid +",'"+ points.getPickupPoint().split("###")[0] +"',"+1+",NOW(),0,1,"+points.getPickupPoint().split("###")[1]+","+ points.getPickupPoint().split("###")[2]+")" ;
				}else{
					query = "insert into pickuppoints (routeid,pickuppoint,isactive,createdon,isorigin,isdestination,latitude,longitude)"+
						"	values ("+ routeid +",'"+ points.getPickupPoint().split("###")[0] +"',"+1+",NOW(),0,0,"+points.getPickupPoint().split("###")[1]+","+ points.getPickupPoint().split("###")[2]+")" ;
				}
				stmt.addBatch(query);i++;
			}
						
			/*query = "insert into pickuppoints (routeid,pickuppoint,isactive,createdon,isorigin,isdestination,latitude,longitude)"+
					"	values ("+ routeid +",'"+ destination +"',"+1+",NOW(),0,1)" ;
			stmt.addBatch(query);*/
			
			stmt.executeBatch();
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	public JSONObject fetchMyPaths(int userid) {
		Connection con = null;
		ArrayList<RouteVO> routes = new ArrayList();
		RouteVO route ;
		JSONObject routesJSON = new JSONObject();
		String query = "select routeid,providedby,origin,destination,starttime, isactive, createdon,capacity from routesprovided "
				+ "	where providedby = ? order by routeid desc";
		PreparedStatement stmt = null ;
		try{
			con = DB.getConnection();
			stmt = con.prepareStatement(query);
			stmt.setInt(1, userid);
			
			ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
            	route = new RouteVO();
            	route.setRouteid(rs.getInt(1)); route.setProvidedby(rs.getInt(2)); route.setOrigin(rs.getString(3));route.setDestination(rs.getString(4));
            	route.setStarttime(rs.getString(5));route.setIsActive(rs.getInt(6));route.setCreatedOn(rs.getTimestamp(7));route.setCapacity(rs.getInt(8));
            	
            	routes.add(route);
            	
            	
            }
			
			routesJSON.put("routes",routes.toArray());
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return routesJSON;
	}

	public JSONObject showMeARoute(int routeid) {
		Connection con = null;
		ArrayList<RouteVO> routes = new ArrayList();
		ArrayList<PickUpPointsVO> points = new ArrayList();
		RouteVO route = null ;PickUpPointsVO point ;
		JSONObject routesJSON = new JSONObject();
		String query = "select routeid,providedby,origin,destination,starttime, isactive, createdon,capacity,username from routesprovided,users "
				+ "	where routeid = ? and routesprovided.providedby = users.userid order by routeid desc";
		
		String queryPoints = "select pickuppointid,pickuppoint,isorigin,isdestination,latitude,longitude from pickuppoints "
				+ "	where routeid = ? and isactive =1 order by pickuppointid asc";
		
		PreparedStatement stmt = null ;
		try{
			con = DB.getConnection();
			stmt = con.prepareStatement(query);
			stmt.setInt(1, routeid);
			
			ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
            	route = new RouteVO();
            	route.setRouteid(rs.getInt(1)); route.setProvidedby(rs.getInt(2)); route.setOrigin(rs.getString(3));route.setDestination(rs.getString(4));
            	route.setStarttime(rs.getString(5));route.setIsActive(rs.getInt(6));route.setCreatedOn(rs.getTimestamp(7));route.setCapacity(rs.getInt(8));
            	route.setUsername(rs.getString(9));
            	
            }
            
            stmt = con.prepareStatement(queryPoints); 
            stmt.setInt(1, routeid);
            
            rs = stmt.executeQuery();
            while(rs.next())
            {
            	point = new PickUpPointsVO();
            	point.setPickuppointid(rs.getInt(1));point.setPickupPoint(rs.getString(2));point.setIsOrigin(rs.getInt(3));point.setIsDestination(rs.getInt(4));
            	point.setLatitude(rs.getDouble(5));point.setLongitude(rs.getDouble(6));
            	points.add(point);
            }
			
            if(route != null){
            	PickUpPointsVO [] pointsArr = new PickUpPointsVO[points.size()];
            	for(int i=0;i<points.size();i++){
            		pointsArr[i] = points.get(i);
            	}
            	
            	route.setPickupPoints(pointsArr);
            }
            
            
			routesJSON = new JSONObject(route);
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return routesJSON;
	}
 
	/*****************
	 * 1. First see the routes with matching pickup points or origin = origin
	 * 2. Check routes with matching pickup points or destination = destination
	 * 3. then take intersection of routes
	 * 4. finally check direction by comparingpickuppointid
	 * *************************/
	
	public JSONObject fetchMyMatchedRoutesToday(RouteVO routesToday) {
		Connection con = null;
		ArrayList<PickUpPointsVO> originBoundRoutes = new ArrayList();
		ArrayList<PickUpPointsVO> destinationBoundRoutes = new ArrayList();
		ArrayList<RouteVO> mathingRoutes = new ArrayList<RouteVO>();
		RouteVO route = null;
		PickUpPointsVO pointVO = null;
		JSONObject routesJSON = new JSONObject();
		String query = "select routeid,isorigin,isdestination,pickuppoint from pickuppoints where pickuppoint = ? and createdon >= CURDATE()"
												+" and createdon < (CURDATE() + INTERVAL 1 DAY)";
				
		PreparedStatement stmt = null ;
		try{
			con = DB.getConnection();
			stmt = con.prepareStatement(query);
			stmt.setString(1, routesToday.getOrigin());
			
			ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
            	pointVO = new PickUpPointsVO();
            	pointVO.setRouteid(rs.getInt(1));pointVO.setIsOrigin(rs.getInt(2));pointVO.setIsDestination(rs.getInt(3));pointVO.setPickupPoint(rs.getString(4));
            	
            	originBoundRoutes.add(pointVO);
            }
            
            stmt = con.prepareStatement(query);
			stmt.setString(1, routesToday.getDestination());
			
			rs = stmt.executeQuery();
            while(rs.next())
            {
            	pointVO = new PickUpPointsVO();
            	pointVO.setRouteid(rs.getInt(1));pointVO.setIsOrigin(rs.getInt(2));pointVO.setIsDestination(rs.getInt(3));pointVO.setPickupPoint(rs.getString(4));
            	
            	destinationBoundRoutes.add(pointVO);
            }
			
            //Compare them and intersect
            ArrayList<PickUpPointsVO> matchingRoutes = new ArrayList<PickUpPointsVO>();
            for(PickUpPointsVO tempDestVO: destinationBoundRoutes ){
            	for(PickUpPointsVO tempOrigVO: originBoundRoutes ){
            		if(tempDestVO.getRouteid() == tempOrigVO.getRouteid()){
            			matchingRoutes.add(tempDestVO);
            		}
            	}
            }
            
            //Check Directions and further filter routes
            ArrayList<Integer> directedMatchingRoutes = new ArrayList<Integer>();
            String queryPoints = "select routeid, pickuppointid,pickuppoint from pickuppoints where routeid = ? and isactive =1 and pickuppoint in (?,?) order by pickuppointid asc";
            for(PickUpPointsVO tempVO: matchingRoutes){
            	stmt = con.prepareStatement(queryPoints);
            	stmt.setInt(1, tempVO.getRouteid());stmt.setString(2, routesToday.getOrigin());stmt.setString(3, routesToday.getDestination());
    			int originid = 0;
    			int destinationid = 0;
    			
    			rs = stmt.executeQuery();
                while(rs.next())
                {
                	if(rs.getString(3).equalsIgnoreCase(routesToday.getDestination())){
                		destinationid = rs.getInt(2);
                	}
                	if(rs.getString(3).equalsIgnoreCase(routesToday.getOrigin())){
                		originid = rs.getInt(2);
                	}
                }
                if(originid < destinationid)
            		directedMatchingRoutes.add(tempVO.getRouteid());
            }
            
            String queryRoute = "select routeid,providedby,origin,destination,starttime, isactive, createdon,capacity from routesprovided "
    				+ "	where routeid in ";
    		
    			con = DB.getConnection();
    			
    			String inqueryRoute = "(";
    			
    			for(Integer i:directedMatchingRoutes){
    				inqueryRoute += i +",";
    			}
    			
    			inqueryRoute = inqueryRoute.substring(0,inqueryRoute.length()-1)+") order by routeid desc"; 
    			
    			stmt = con.prepareStatement(queryRoute + inqueryRoute);
    			
    			rs = stmt.executeQuery();
                while(rs.next())
                {
                	route = new RouteVO();
                	route.setRouteid(rs.getInt(1)); route.setProvidedby(rs.getInt(2)); route.setOrigin(rs.getString(3));route.setDestination(rs.getString(4));
                	route.setStarttime(rs.getString(5));route.setIsActive(rs.getInt(6));route.setCreatedOn(rs.getTimestamp(7));route.setCapacity(rs.getInt(8));
                	
                	mathingRoutes.add(route);
                	
                	
                }
    			
    			routesJSON.put("routes",mathingRoutes.toArray());
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return routesJSON;
	}

	public JSONObject fetchPointsToday() {
		Connection con = null;
		ArrayList<String> points = new ArrayList<String>();
		JSONObject pointsJSON = new JSONObject();
		String query = "select distinct pickuppoint from pickuppoints where createdon >= CURDATE() and createdon < (CURDATE() + INTERVAL 1 DAY) order by pickuppoint asc";
		PreparedStatement stmt = null ;
		try{
			con = DB.getConnection();
			stmt = con.prepareStatement(query);
			
			ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
            	points.add(rs.getString(1));
            }
			
            pointsJSON.put("points",points.toArray());
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return pointsJSON;
	}

	public void bookThisRouteForMe(RouteVO routesToday,String otp) {
		Connection con = null;
		
		PreparedStatement stmt = null ;
		try{
			
			con = DB.getConnection();
			con.setAutoCommit(false);
			
			String query = "insert into routesselected (routeid,subscriberid,isactive,createdon,pickuppoint) values (?,?,1,Now(),?)" ;
			stmt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			
			stmt.setInt(1,routesToday.getRouteid()); stmt.setInt(2,Integer.parseInt(routesToday.getUsername()));stmt.setString(3,routesToday.getOrigin());
			
			stmt.executeUpdate();
			
			ResultSet rs = stmt.getGeneratedKeys();
			int last_inserted_id = 0;
            if(rs.next())
            {
                last_inserted_id = rs.getInt(1);
            }
			
			query = "insert into otpstore (otp,senttoreceiverid,providerid,journeyid, action,createdon,authenticated,authenticatedon,selectionid)"
					+ " values (?,?,?,?,?,Now(),?,?)" ;
			stmt = con.prepareStatement(query);
			stmt.setString(1, otp);stmt.setString(2, routesToday.getUsername());stmt.setString(3,""+routesToday.getProvidedby());stmt.setString(4, routesToday.getRouteid()+"");
			stmt.setString(5,"root_booking");stmt.setInt(6,0);stmt.setTimestamp(7,null);stmt.setString(8, ""+last_inserted_id);
			
			stmt.execute();
			
			con.commit();
			con.setAutoCommit(true);
		}catch(Exception e){
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally{
			try {
				con.setAutoCommit(true);
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	public List<SubscriberVO> getSubscribers(int routeid) {
		Connection con = null;
		SubscriberVO objSub = null;
		ArrayList<SubscriberVO> subscribers = new ArrayList<SubscriberVO>();
		//JSONObject subscriberJSON = new JSONObject();
		String query = "SELECT selectionid,routeid,subscriberid,isactive,pickuppoint,imageurl,username FROM routesselected,users "+
							"where users.userid = routesselected.subscriberid and routeid = ? and isactive = 1";
		PreparedStatement stmt = null ;
		try{
			con = DB.getConnection();
			stmt = con.prepareStatement(query);
			stmt.setInt(1, routeid);
			
			ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
            	objSub = new SubscriberVO();
            	objSub.setSelectionid(rs.getInt(1));objSub.setRouteid(rs.getInt(2));objSub.setUserid(rs.getInt(3));objSub.setPickuppoint(rs.getString(5));
            	objSub.setImageurl(rs.getString(6));objSub.setUsername(rs.getString(7));
            	
            	subscribers.add(objSub);
            }
			
            //subscriberJSON.put("subscribers",subscribers.toArray());
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return subscribers;
	}

	public SubscriberVO getSubscriber(int subid) {
		Connection con = null;
		SubscriberVO objSub = null;
		
		String query = "SELECT selectionid,routeid,subscriberid,isactive,pickuppoint,imageurl,username FROM routesselected,users "+
							"where users.userid = routesselected.subscriberid and subscriberid = ? and isactive = 1";
		PreparedStatement stmt = null ;
		try{
			con = DB.getConnection();
			stmt = con.prepareStatement(query);
			stmt.setInt(1, subid);
			
			ResultSet rs = stmt.executeQuery();
            while(rs.next())
            {
            	objSub = new SubscriberVO();
            	objSub.setSelectionid(rs.getInt(1));objSub.setRouteid(rs.getInt(2));objSub.setUserid(rs.getInt(3));objSub.setPickuppoint(rs.getString(5));
            	objSub.setImageurl(rs.getString(6));objSub.setUsername(rs.getString(7));
            	
            }
			
            //subscriberJSON.put("subscribers",subscribers.toArray());
		
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return objSub;
	}

	public void createTrip(TripVO trip) throws Exception {
		/*Connection con = null;
		String query = "insert into tripstore (routeid,createdon,status) values (?,NOW(),1)" ;
		PreparedStatement stmt = null ;
		int last_inserted_id = 0;
		try{
			con = DB.getConnection();
			con.setAutoCommit(false);
			
			stmt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, trip.getRouteid());
						
			stmt.executeUpdate();
			
			ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next())
            {
                last_inserted_id = rs.getInt(1);
            }
			
			if(last_inserted_id != 0){
				
				for(int i:trip.getSubscriberids()){
					query = "insert into tripsubscriberrel (tripid,subscriberid, status) values ("+ last_inserted_id +","+ i +",1)" ;
					stmt.addBatch(query);
				}
				
				stmt.executeBatch();
			}
			con.commit();	
			con.setAutoCommit(true);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				con.setAutoCommit(true);
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
		
		
	}

}
