package com.stream.dao;

import java.util.List;

import org.json.JSONObject;

import com.stream.exception.StreamCustomException;
import com.stream.vo.LoginVO;
import com.stream.vo.RegistrationVO;
import com.stream.vo.UserVO;

public interface IUserManagementDAO {
	
	public UserVO authenticate(UserVO userObj);

			
}
