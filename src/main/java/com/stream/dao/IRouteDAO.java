package com.stream.dao;

import java.util.List;

import org.json.JSONObject;

import com.stream.vo.SubscriberVO;
import com.stream.vo.TripVO;
import com.stream.vo.UserVO;
import com.stream.vo.PickUpPointsVO;
import com.stream.vo.RouteVO;

public interface IRouteDAO {

	public RouteVO createRoute(RouteVO routeVO);

	public void createPickupPoints(PickUpPointsVO[] pickupPoints, int routeid, String origin, String destination);

	public JSONObject fetchMyPaths(int userid);

	public JSONObject showMeARoute(int routeid);

	public JSONObject fetchMyMatchedRoutesToday(RouteVO routesToday);

	public JSONObject fetchPointsToday();

	public void bookThisRouteForMe(RouteVO routesToday,String otp);

	public List<SubscriberVO> getSubscribers(int routeid);

	public SubscriberVO getSubscriber(int subid);

	public void createTrip(TripVO trip) throws Exception;
	
}