package com.stream.dao;

import com.stream.common.ResponseTypes;
import com.stream.common.Utility;
import com.stream.db.DB;
import com.stream.exception.StreamCustomException;
import com.stream.vo.LoginVO;
import com.stream.vo.RegistrationVO;
import com.stream.vo.UserVO;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;


public class UserManagementDAO implements IUserManagementDAO{
	
	
	
	public UserVO authenticate(UserVO userObj) {
		Connection con = null;
		String query = "select userid,username,mobileno,deviceid from users where mobileno= ? and password = ? ";
		UserVO objUser = null;
		PreparedStatement stmt = null ;
		try{
			con = DB.getConnection();
			stmt = con.prepareStatement(query);
			stmt.setString(1, userObj.getMobilenumber());
			stmt.setString(2, userObj.getPassword());
			
			ResultSet rs = stmt.executeQuery();
			if(rs != null){
				
				while(rs.next()) {
					objUser = new UserVO();
					objUser.setUserid(rs.getInt("userid"));
					objUser.setUsername(rs.getString("username"));
					objUser.setMobilenumber(rs.getString("mobileno"));
					objUser.setLoginstatus("success");
					
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return objUser;
		
	}
		
}
