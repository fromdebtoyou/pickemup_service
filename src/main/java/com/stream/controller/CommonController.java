package com.stream.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.stream.db.DB;
import com.stream.vo.DensityVO;
import com.stream.vo.RouteRequest;

@Path("/common")
public class CommonController {
	

	@GET
	@Produces("application/json")
    @Path("/test")
	public Response fetchPoints1(){
		return Response.status(200).entity("Test").build();	
	}
	
	@GET
	@Produces("application/json")
    @Path("/showPoints")
	public Response fetchPoints(){
		System.out.println("test");
		JSONArray jsonArray = null;
		JSONObject jsonObject = null;
		String sqlStatement = "select routepoints.RoutePointId, routepoints.Description from routepoints";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConnection();
			pstmt = conn.prepareStatement(sqlStatement);
			
			rs = pstmt.executeQuery();
			
			if(rs != null) {
				jsonArray = new JSONArray();
				while(rs.next()){
					jsonObject = new JSONObject();
					
					jsonObject.put("id", rs.getInt(1));
					jsonObject.put("description", rs.getString(2));
					
					jsonArray.put(jsonObject);
				}
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/****************************************/
		
		return Response.status(200).entity(jsonArray.toString()).build();		
	}
	
	@GET
	@Produces("application/json")
    @Path("/whereToGo")
	public Response getPossibleDestination(@QueryParam(value = "routepoint") int routepointid){

		JSONArray jsonArray = null;
		JSONObject jsonObject = null;
		String sqlStatement = "select distinct routepoints.RoutePointId, routepoints.Description "
				+ "from routepoints,routelinks "
				+ "where  routepoints.RoutePointId = routelinks.RoutePointId "
				+ "and routelinks.RouteId in ( "
				+ "select routes.RouteId from routes,routelinks where routelinks.RoutePointId = ? and routelinks.RouteId = routes.RouteId"
				+ ") order by Description asc;";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.getConnection();
			pstmt = conn.prepareStatement(sqlStatement);
			pstmt.setInt(1, routepointid);
			
			rs = pstmt.executeQuery();
			
			if(rs != null) {
				jsonArray = new JSONArray();
				while(rs.next()){
					jsonObject = new JSONObject();
					
					jsonObject.put("id", rs.getInt(1));
					jsonObject.put("description", rs.getString(2));
					
					jsonArray.put(jsonObject);
				}
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/****************************************/
		
		return Response.status(200).entity(jsonArray.toString()).build();		
	}
	
	@GET
	@Produces("application/json")
    @Path("/getRouteTags")
	public Response getPossibleRoutes(@QueryParam(value = "start") int startid,@QueryParam(value = "end") int endid){

		
		String sqlStatement = "select routes.RouteId, routes.RouteName,routelinks.RoutePointId,routelinks.sequence "
				+ "from routes,routelinks where routelinks.RoutePointId in(?,?) and routelinks.RouteId = routes.RouteId;";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Map<String,Map> routeMap = null;
		List<Integer> routes = null;
		JSONArray jsonArray = null;
		JSONObject jsonObject = null;
		try {
			conn = DB.getConnection();
			pstmt = conn.prepareStatement(sqlStatement);
			pstmt.setInt(1, startid);
			pstmt.setInt(2, endid);
			
			rs = pstmt.executeQuery();
			
			if(rs != null) {
				routeMap = new HashMap<String,Map>(); 
				routeMap = populateRouteMap(rs);
				
				routes = getRouteTagging(startid, endid, routeMap);
			}
		
			String getRouteDesc = "select RouteId, RouteName "
						+ "from routes where RouteId in ";
			String routeList = "(";
			for(Integer s : routes){
				routeList += s+",";
			}	
			routeList = routeList.substring(0, routeList.length()-1); routeList = routeList+")";
			getRouteDesc = getRouteDesc + routeList;
		
			pstmt = conn.prepareStatement(getRouteDesc);
			
			rs = pstmt.executeQuery();
			
			if(rs != null) {
				jsonArray = new JSONArray();
				while(rs.next()){
					jsonObject = new JSONObject();
					
					jsonObject.put("id", rs.getInt(1));
					jsonObject.put("description", rs.getString(2));
					
					jsonArray.put(jsonObject);
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/****************************************/
		
		return Response.status(200).entity(jsonArray.toString()).build();		
	}

	private List<Integer> getRouteTagging(int startid, int endid,
			Map<String, Map> routeMap) {
		List<Integer> routes = new ArrayList<Integer>();
		if(routeMap != null){
			routeMap.forEach((key, value) -> {
			    String routeId = key;
			    if(value.get(""+startid) != null && value.get(""+endid) != null && (Integer)value.get(""+startid) <= (Integer)value.get(""+endid)){
			    	routes.add(Integer.parseInt(routeId));
			    }
			});
		}
		return routes;
	}

	private Map<String, Map> populateRouteMap(ResultSet rs)
			throws SQLException {
		Map<String, Map> routeMap;
		Map<String, Integer> sequenceMap = null;
		routeMap = new HashMap<String,Map>();
		while(rs.next()){
			if(routeMap.get(""+rs.getInt(1))!=null){
				sequenceMap = new HashMap<String,Integer>();
				sequenceMap = routeMap.get(""+rs.getInt(1));
			}else{
				sequenceMap = new HashMap<String,Integer>();
			}
			sequenceMap.put(""+rs.getInt(3) ,rs.getInt(4) );
			routeMap.put(""+rs.getInt(1), sequenceMap);
		}
		return routeMap;
	}
	
	@GET
	@Produces("application/json")
    @Path("/checkOrderInoute")
	public Response checkOrderInoute(@QueryParam(value = "start") int startid,@QueryParam(value = "end") int endid,@QueryParam(value = "route") int routeid){

		String sqlStatement = "select routelinks.RoutePointId,routelinks.Sequence from routelinks where routelinks.RouteId = ? "
				+ "and routelinks.RoutePointId in (?,?) order by Sequence asc";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean isAhead = false;
		try {
			conn = DB.getConnection();
			pstmt = conn.prepareStatement(sqlStatement);
			pstmt.setInt(1, routeid);
			pstmt.setInt(2, startid);
			pstmt.setInt(3, endid);
			
			rs = pstmt.executeQuery();
			
			if(rs != null && rs.next()) {
				if(startid == rs.getInt(1)){
					isAhead = true;
				}
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return Response.status(200).entity("{ \"response\":"+ isAhead +"}").build();		
	}
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@Path("/saveRouteRequest")
	public Response saveRouteRequest(RouteRequest request){

		String sqlStatement = "insert into requestpool (userid,startpointid,endpointid,timestart,createdon) values(?,?,?,?,NOW())";

		Connection conn = null;
		PreparedStatement pstmt = null;
		
		String status = "Save failed";
		
		String sqlStatementRouteSuggestion = "select routes.RouteId, routes.RouteName,routelinks.RoutePointId,routelinks.sequence "
				+ "from routes,routelinks where routelinks.RoutePointId in(?,?) and routelinks.RouteId = routes.RouteId;";
		ResultSet rs = null;
		Map<String,Map> routeMap = null;
		List<Integer> routes = null;
		Map<String,DensityVO> densityMap = null;
		Integer requestPoolId = 0;
		try {
			conn = DB.getConnection();
			conn.setAutoCommit(false);
			
			pstmt = conn.prepareStatement(sqlStatement,Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, request.getUserId());
			pstmt.setInt(2, request.getStartPointId());
			pstmt.setInt(3, request.getEndPointId());
			pstmt.setString(4, request.getJourneyTime());
			
			pstmt.executeUpdate();
			
			rs = pstmt.getGeneratedKeys();
            if(rs.next())
            {
            	requestPoolId = rs.getInt(1);
            }
			
			pstmt = conn.prepareStatement(sqlStatementRouteSuggestion);
			pstmt.setInt(1, request.getStartPointId());
			pstmt.setInt(2, request.getEndPointId());
				
			rs = pstmt.executeQuery();
			
			if(rs != null) {
				routeMap = new HashMap<String,Map>(); 
				routeMap = populateRouteMap(rs);
				
				routes = getRouteTagging(request.getStartPointId(), request.getEndPointId(), routeMap);
			}
			
			if(routes !=null && routes.size()>0){
				
				String journeyHour = request.getJourneyTime().split(":")[0]; 
				String routelist = "";
				for(Integer s:routes){
					routelist += s+",";
				}
				routelist = routelist.substring(0,routelist.length()-1);
				String queryRoute = "SELECT RouteId,RoutePointId,Sequence from routelinks where RouteId in ("+ routelist  +") and RoutePointId in (?,?) order by RouteId;" ;
				pstmt = conn.prepareStatement(queryRoute); pstmt.setInt(1, request.getStartPointId());pstmt.setInt(2, request.getEndPointId());
				
				rs = pstmt.executeQuery();
				
				densityMap = new HashMap<String, DensityVO>();
				DensityVO objDensity = null;
				while(rs != null && rs.next()){
					objDensity = densityMap.get(""+rs.getInt(1));
					if(objDensity == null){
						objDensity = new DensityVO();
						objDensity.setRouteid(rs.getInt(1));		
					}
					if(rs.getInt(2) == request.getStartPointId()){
						objDensity.setStartseq(rs.getInt(3));
					}else{
						objDensity.setEndseq(rs.getInt(3));
					}
					densityMap.put(""+rs.getInt(1), objDensity);
				}
				
				for(Integer s:routes){
					String query = "insert into requestdensity (routeid,count,starttime,endtime,startseq,endseq,requestpoolid) values(\""+ s + "\",1,\""+ Integer.parseInt(journeyHour) +
							":00\",\""+ Integer.parseInt(journeyHour)+":59\","+ densityMap.get(""+s).getStartseq() +","+ densityMap.get(""+s).getEndseq() +","+ requestPoolId +")";
					pstmt.addBatch(query);
				} 
				
				pstmt.executeBatch();
				
				status = "Save successful";
			}
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				conn.setAutoCommit(true);
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return Response.status(200).entity("{ \"response\":\""+ status +"\"}").build();	
		
			
	}

}
