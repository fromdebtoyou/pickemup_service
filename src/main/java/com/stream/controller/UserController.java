package com.stream.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.stream.exception.StreamCustomException;
import com.stream.facade.IUserManagementFacade;
import com.stream.facade.UserManagementFacade;
import com.stream.vo.LoginVO;
import com.stream.vo.UserVO;


@Path("/pickemup")
public class UserController {
	
	IUserManagementFacade userFacade = new UserManagementFacade();
	
	@POST
	@Consumes ("application/json")
	@Produces("application/json")
    @Path("/login")
	public Response login(LoginVO loginVO ){

		UserVO userObj = new UserVO();
		userObj.setMobilenumber(loginVO.getUsername());
		userObj.setPassword(loginVO.getPassword());
		userObj.setDeviceid(loginVO.getDevicetype());
		
		JSONObject jsonObject = userFacade.login(userObj);
		
		if(jsonObject == null) {
			jsonObject = new JSONObject();
			jsonObject.put("operation", "login");
			jsonObject.put("status", "failure");
			jsonObject.put("errorcode", StreamCustomException.LOGIN_FAILURE_CODE);
			jsonObject.put("errormessage", StreamCustomException.LOGIN_FAILURE_MESSAGE);
		}else{
			jsonObject.put("operation", "login");
			if(jsonObject.has("errormessage") && jsonObject.getString("errormessage").equalsIgnoreCase(StreamCustomException.LOGIN_FAILURE_WRONG_MACID_MESSAGE)){
				jsonObject.put("status", "failure");
			}else{
				jsonObject.put("status", "success");
			}
		}
		jsonObject.put("uid", loginVO.getUsername());
		return Response.status(200).entity(jsonObject.toString()).build();		
	}

}
