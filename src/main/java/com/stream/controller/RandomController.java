package com.stream.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.dbcp.DbcpException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.stream.common.Utility;
import com.stream.db.DB;
import com.stream.vo.KeyValue;
import com.stream.vo.RouteVO;
import com.stream.vo.TripVO;


@Path("/random")
public class RandomController {
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
    @Path("/getRouteDesnity")
	public Response getRouteDesnity(KeyValue[] keyValArray){
		JSONObject densities = new JSONObject();
		JSONArray densitiesPerRoute = null;
		JSONObject density = null;
		String kvlString  = "(";
		for(KeyValue kvl: keyValArray){
			kvlString +=kvl.getKey()+",";
			densities.put(kvl.getKey(), new JSONArray());
		}
		kvlString = kvlString.substring(0,kvlString.length()-1); kvlString += ")";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		String sqlGetDensity = "SELECT routeid,count(1),starttime,endtime FROM pickemup.requestdensity where routeid in "+ kvlString +
				" group by starttime,routeid,endtime order by routeid asc";
		ResultSet rs = null;
		
		try{
			conn = DB.getConnection();
			pstmt = conn.prepareStatement(sqlGetDensity);
			rs = pstmt.executeQuery();
			if(rs != null){
				while(rs.next()){
					if(densities.get(""+rs.getInt(1)) == null){
						densitiesPerRoute = new JSONArray();
					}else{
						densitiesPerRoute = (JSONArray) densities.get(""+rs.getInt(1));
					}
					
					density = new JSONObject();
					density.put("count", rs.getInt(2));density.put("starttime", rs.getString(3));density.put("endtime", rs.getString(4));
					densitiesPerRoute.put(density);
					densities.put(""+rs.getInt(1), densitiesPerRoute);
				}
			}
			
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return Response.status(200).entity(densities.toString()).build();

	}
	
	@GET
	@Produces("application/json")
    @Path("/getUsersForARouteAndSlot")
	public Response getUsersForARouteAndSlot(@QueryParam(value = "starttime") String starttime,@QueryParam(value = "routeId") int routeId){
		JSONObject user = new JSONObject();
		JSONArray userRequests = null;
		JSONObject compositeResponse = null;
				
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		String sqlGetDensity = "SELECT req.rqeuestid, req.userid, us.username, us.imageurl, req.startpointid, rp1.description as origin, req.endpointid, rp2.description as destination, "
				+ "req.timestart from requestpool req,routepoints rp1, routepoints rp2, requestdensity rd, users us "
				+ "where req.startpointid = rp1.RoutePointId and req.endpointid = rp2.RoutePointId and us.userid=req.userid and req.timestart Like '"+starttime+":%' "
						+ "and req.rqeuestid = rd.requestpoolid and rd.routeid = ?";
		ResultSet rs = null;
		
		try{
			//TBD
			conn = DB.getConnection();
			pstmt=conn.prepareStatement(sqlGetDensity);
			pstmt.setInt(1, routeId);
			rs=pstmt.executeQuery();
			userRequests=new JSONArray();
			while(rs.next()){
				user = new JSONObject();
				user.put("rqeuestid", rs.getString("rqeuestid"));
				user.put("userid", rs.getString("userid"));
				user.put("username", rs.getString("username"));
				user.put("imageurl", rs.getString("imageurl"));
				user.put("startpointid", rs.getString("startpointid"));
				user.put("origin", rs.getString("origin"));
				user.put("endpointid", rs.getString("endpointid"));
				user.put("destination", rs.getString("destination"));
				user.put("timestart", rs.getString("timestart"));
				userRequests.put(user);
			}
						
		}catch(Exception ex){
			ex.printStackTrace();
		}		
		return Response.status(200).entity(userRequests.toString()).build();
		
	}
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
    @Path("/saveTrip")
	public Response saveTripByProvider(TripVO trip){
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		String sqlSaveTrip = "insert into tripstore (routeid,createdon,status,providedby,tripstarttime,tripstartpoint,tripendpoint,carmake,carmodel)"+
						" values(?,NOW(),0,?,?,?,?,?,?)";
		
		String sqlSaveTripRequestRel = "insert into triprequestmap (tripid,requestpoolid,status)  values(?,?,?)";
		ResultSet rs = null;
		int tripId =0 ;
		
		try{
			conn = DB.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(sqlSaveTrip, Statement.RETURN_GENERATED_KEYS);
			Timestamp timestamp ;
			timestamp = dateExtractor(trip);
		    
			pstmt.setInt(1, trip.getRouteid());pstmt.setInt(2, trip.getProvidedBy());pstmt.setTimestamp(3, timestamp);pstmt.setInt(4, trip.getStartPoint());
			pstmt.setInt(5, trip.getEndPoint());pstmt.setString(6, trip.getCarmake());pstmt.setString(7, trip.getCarmodel());
			
			pstmt.executeUpdate();
			
			rs = pstmt.getGeneratedKeys();
            if(rs.next())
            {
            	tripId = rs.getInt(1);
            }
            
            String[] reqIds = trip.getRequestPoolId().split(",");
            for(String s: reqIds){
            	String query = "insert into triprequestmap (tripid,requestpoolid,status)  values(" + tripId + "," + Integer.parseInt(s) + "," + "0)";
				pstmt.addBatch(query);
            }
            
            pstmt.executeBatch();
            
            //Store OTPs in OPSTore
            for(String s: reqIds){
            	String otp = Utility.otpGenerator();
            	String OTPQuery = "insert into otpstore (otp,requestpoolid,tripid, action,createdon)"
    					+ " values ("+ otp +"," + Integer.parseInt(s) + "," + tripId +",\" Route Set\",NOW())" ;
				pstmt.addBatch(OTPQuery);
            }
            pstmt.executeBatch();
            
            conn.commit();
                        
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try {
				conn.setAutoCommit(true);
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return Response.status(200).entity("{\"status\":\"successful\"}").build();

	}

	private Timestamp dateExtractor(TripVO trip) throws ParseException {
		Timestamp timestamp;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println(cal.getTime());
		String formatted = format1.format(cal.getTime());
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		Date parsedTimeStamp = dateFormat.parse(formatted + " "+ trip.getStartTime() +":59:59");

		timestamp = new Timestamp(parsedTimeStamp.getTime());
		
		return timestamp;
	}
	

}
