package com.stream.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Servlet implementation class PropertiesLoader
 */
@Path("/prop")
public class PropertiesLoader {
	private static final long serialVersionUID = 1L;
	
	private static Properties properties = null;
	
	public static Properties getProperties(){
		return properties;
	}
       
        
    
    static{
    	properties = new Properties();
    	try {
				ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
				properties.load(classLoader.getResourceAsStream("config.properties"));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
    	
    }

    @GET
    @Path("/load")
	public Response load(){
		
    	try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			properties.load(classLoader.getResourceAsStream("config.properties"));
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	};
		
		return Response.status(200).entity("Successfully loaded").build();
	}

}
