package com.stream.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.stream.facade.IRouteFacade;
import com.stream.facade.RouteFacade;
import com.stream.vo.RouteVO;
import com.stream.vo.TripVO;
import com.stream.vo.UserVO;


@Path("/route")
public class ProviderController {
	
	IRouteFacade routeFacade = new RouteFacade();
	
	@POST
	@Consumes ("application/json")
	@Produces("application/json")
    @Path("/createpath")
	public Response createPath(RouteVO routeVO ){

		JSONObject jsonObject = routeFacade.createPath(routeVO);
		int routeid = 0;
		
		if(jsonObject == null) {
			jsonObject = new JSONObject();
			jsonObject.put("operation", "createpath");
			jsonObject.put("status", "failure");
		}else{
			jsonObject.put("operation", "createpath");
			jsonObject.put("status", "success");
			routeid = jsonObject.getInt("routeid");
		}
		
		routeFacade.createPickupPoints(routeVO.getPickupPoints(),routeid,routeVO.getOrigin(),routeVO.getDestination());
		
		return Response.status(200).entity(jsonObject.toString()).build();		
	}
	
	@GET
	@Produces("application/json")
    @Path("/fetchmyroutes")
	public Response fetchPaths(@QueryParam(value = "userid") int userid){

		JSONObject jsonObject = routeFacade.fetchMyPaths(userid);
		
		if(jsonObject == null) {
			jsonObject = new JSONObject();
			jsonObject.put("operation", "fetchMyPaths");
			jsonObject.put("status", "failure");
		}else{
			jsonObject.put("operation", "fetchMyPaths");
			jsonObject.put("status", "success");
		}
		
		return Response.status(200).entity(jsonObject.toString()).build();		
	}
	
	@GET
	@Produces("application/json")
    @Path("/showmearoute")
	public Response showMeARoute(@QueryParam(value = "routeid") int routeid){

		JSONObject jsonObject = routeFacade.showMeARoute(routeid);
		
		if(jsonObject == null) {
			jsonObject = new JSONObject();
			jsonObject.put("operation", "showMeARoute");
			jsonObject.put("status", "failure");
		}else{
			jsonObject.put("operation", "showMeARoute");
			jsonObject.put("status", "success");
		}
		
		return Response.status(200).entity(jsonObject.toString()).build();		
	}
	
	@GET
	@Produces("application/json")
    @Path("/getsuscriberslist")
	public Response getSubscribers(@QueryParam(value = "routeid") int routeid){

		JSONObject jsonObject = routeFacade.getSubscribers(routeid);
		
		if(jsonObject == null) {
			jsonObject = new JSONObject();
			jsonObject.put("operation", "getSubscribers");
			jsonObject.put("status", "failure");
		}else{
			jsonObject.put("operation", "getSubscribers");
			jsonObject.put("status", "success");
		}
		
		return Response.status(200).entity(jsonObject.toString()).build();		
	}
	
	@GET
	@Produces("application/json")
    @Path("/getindividualsubscriber")
	public Response getOneSubscriber(@QueryParam(value = "subscriberid") int subid){

		JSONObject jsonObject = routeFacade.getSubscriber(subid);
		
		if(jsonObject == null) {
			jsonObject = new JSONObject();
			jsonObject.put("operation", "getOneSubscriber");
			jsonObject.put("status", "failure");
		}else{
			jsonObject.put("operation", "getOneSubscriber");
			jsonObject.put("status", "success");
		}
		
		return Response.status(200).entity(jsonObject.toString()).build();		
	}
	
	@POST
	@Consumes ("application/json")
	@Produces("application/json")
    @Path("/finalizeroute")
	public Response finalizeRoute(TripVO trip ){

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = routeFacade.createTrip(trip);
		
			if(jsonObject == null) {
				jsonObject = new JSONObject();
				jsonObject.put("operation", "finalizeRoute");
				jsonObject.put("status", "failure");
			}else{
				jsonObject.put("operation", "finalizeRoute");
				jsonObject.put("status", "success");
			}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Response.status(200).entity(jsonObject.toString()).build();
				
	}
	
	/*@POST
	@Consumes ("application/json")
	@Produces("application/json")
    @Path("/starttrip")
	public Response startTrip(String tripid ){

		JSONObject jsonObject = routeFacade.createPath(routeVO);
		int routeid = 0;
		
		if(jsonObject == null) {
			jsonObject = new JSONObject();
			jsonObject.put("operation", "createpath");
			jsonObject.put("status", "failure");
		}else{
			jsonObject.put("operation", "createpath");
			jsonObject.put("status", "success");
			routeid = jsonObject.getInt("routeid");
		}
		
		routeFacade.createPickupPoints(routeVO.getPickupPoints(),routeid,routeVO.getOrigin(),routeVO.getDestination());
		
		return Response.status(200).entity(jsonObject.toString()).build();		
	}
	
	@POST
	@Consumes ("application/json")
	@Produces("application/json")
    @Path("/completetrip")
	public Response completeTrip(String tripid ){

		JSONObject jsonObject = routeFacade.createPath(routeVO);
		int routeid = 0;
		
		if(jsonObject == null) {
			jsonObject = new JSONObject();
			jsonObject.put("operation", "createpath");
			jsonObject.put("status", "failure");
		}else{
			jsonObject.put("operation", "createpath");
			jsonObject.put("status", "success");
			routeid = jsonObject.getInt("routeid");
		}
		
		routeFacade.createPickupPoints(routeVO.getPickupPoints(),routeid,routeVO.getOrigin(),routeVO.getDestination());
		
		return Response.status(200).entity(jsonObject.toString()).build();		
	}
	
*/
}
