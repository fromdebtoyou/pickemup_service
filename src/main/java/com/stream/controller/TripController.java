package com.stream.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.stream.common.Utility;
import com.stream.db.DB;


@Path("/intrip")
public class TripController {
	
	@GET
	@Produces("application/json")
    @Path("/startTrip")
	public Response startTrip(@QueryParam(value = "userid") String userid){
		JSONObject request = new JSONObject();
		JSONArray requests = new JSONArray();
		JSONObject tripObject = new JSONObject();
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		int tripid = 0;
		
		String getMyTrip = "select tripid,status,createdon from tripstore where providedby = ? and status =0 order by createdon desc limit 1";
		String startTheTrip = "update tripstore set status = 1 where tripid = ?";
		String getTripDetails = "SELECT trm.tripid,trm.requestpoolid,trm.status as requeststatus,us.username,rpts.Description as pickupPoint, ots.otp,ots.otpid,us.userid FROM triprequestmap trm,requestpool rp,users us,routepoints rpts,otpstore ots where trm.requestpoolid = rp.rqeuestid and rp.userid = us.userid and rpts.RoutePointId = rp.startpointid and ots.tripid = trm.tripid and ots.requestpoolid = trm.requestpoolid and trm.tripid = ?" ;
		ResultSet rs = null;
		
		try{
			conn = DB.getConnection();
			pstmt = conn.prepareStatement(getMyTrip);
			pstmt.setInt(1, Integer.parseInt(userid));
			rs = pstmt.executeQuery();
			if(rs != null){
				while(rs.next()){
					tripid = rs.getInt(1);
				}
			}
			
			if(tripid !=0){
				pstmt = conn.prepareStatement(startTheTrip);
				pstmt.setInt(1, tripid);
				pstmt.execute();
			}
			rs = null;
			pstmt = conn.prepareStatement(getTripDetails);
			pstmt.setInt(1, tripid);
			rs = pstmt.executeQuery();
			while(rs !=null && rs.next()){
				request = new JSONObject();
				request.put("requestpoolid", rs.getInt(2));request.put("journeystatus", rs.getInt(3));request.put("username", rs.getString(4));request.put("pickuppoint", rs.getString(5));request.put("otp", rs.getInt(6));request.put("otpid", rs.getInt(7));request.put("userid", rs.getInt(8));
				requests.put(request);
			}
			tripObject.put("tripid", tripid);
			tripObject.put("passengers", requests);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return Response.status(200).entity(tripObject.toString()).build();

	}
	
	@GET
	@Produces("application/json")
    @Path("/endSingleJourney")
	public Response endTrip(@QueryParam(value = "tripid") String tripid0,
				@QueryParam(value = "requestpoolid") String requestpoolid0,
				@QueryParam(value = "otpid") String otpid0){
		JSONObject request = new JSONObject();
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		int tripid = Integer.parseInt(tripid0);int requestpoolid = Integer.parseInt(requestpoolid0);int otpid = Integer.parseInt(otpid0);
		
		String checkValidity = "select otpid from otpstore where otpid = ? and requestpoolid =? and tripid = ?";
		String otp = Utility.otpGenerator();
		
		ResultSet rs = null;
		
		try{
			conn = DB.getConnection();
			pstmt = conn.prepareStatement(checkValidity);
			pstmt.setInt(1, otpid);pstmt.setInt(2, requestpoolid);pstmt.setInt(3,tripid);
			rs = pstmt.executeQuery();
			if(rs != null){
				while(rs.next()){
					String generateCompletionOTP = "insert into otpstore (otp,requestpoolid,tripid, action,createdon)"
	    					+ " values ("+ otp +"," + requestpoolid + "," + tripid +",\" Completion of Journey\",NOW())" ;
					pstmt = conn.prepareStatement(generateCompletionOTP);
					pstmt.execute();
				}
			}
			
			request.put("tripid", tripid);
			request.put("requestpoolid", requestpoolid);
			request.put("otp", otp);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return Response.status(200).entity(request.toString()).build();

	}
	
	@GET
	@Produces("application/json")
    @Path("/checkCompletion")
	public Response validateCompletion(@QueryParam(value = "tripid") String tripid0,
				@QueryParam(value = "requestpoolid") String requestpoolid0,
				@QueryParam(value = "otp") String otpid0){
		JSONObject request = new JSONObject();
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		int tripid = Integer.parseInt(tripid0);int requestpoolid = Integer.parseInt(requestpoolid0);int otpid = Integer.parseInt(otpid0);
		
		String checkValidity = "select otpid from otpstore where otp = ? and requestpoolid =? and tripid = ?";
		boolean isPresent = false;
		
		ResultSet rs = null;
		
		try{
			conn = DB.getConnection();
			pstmt = conn.prepareStatement(checkValidity);
			pstmt.setInt(1, otpid);pstmt.setInt(2, requestpoolid);pstmt.setInt(3,tripid);
			rs = pstmt.executeQuery();
			if(rs != null){
				while(rs.next()){
					isPresent = true;
				}
			}
			
			if(isPresent){
				request.put("status",0);
				request.put("message","OTP validated successfully");
			}else{
				request.put("status",1);
				request.put("message","OTP validation failed");
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return Response.status(200).entity(request.toString()).build();

	}
	
	
	@GET
	@Produces("application/json")
    @Path("/completeTrip")
	public Response completeTrip(@QueryParam(value = "tripid") String tripid0){
		JSONObject request = new JSONObject();
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		int tripid = Integer.parseInt(tripid0);
		
		String markompletion = "update tripstore set status =2 where tripid = ?";
		ResultSet rs = null;
		
		try{
			conn = DB.getConnection();
			pstmt = conn.prepareStatement(markompletion);
			pstmt.setInt(1, tripid);
			
			int num = pstmt.executeUpdate();
			
			if(num > 0){
				request.put("status",0);
				request.put("message","Trip completed successfully");
			}else{
				request.put("status",1);
				request.put("message","Trip completion failed");
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return Response.status(200).entity(request.toString()).build();

	}
	
	@GET
	@Produces("application/json")
    @Path("/notification")
	public Response getNotofication(@QueryParam(value = "userid") String useridTxt,@QueryParam(value = "type") String type){
		JSONArray jsonArray=new JSONArray();
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		int userid = Integer.parseInt(useridTxt);
		
		String notification = "SELECT t2.tripid,t1.timestart,t3.otp,t3.otpid FROM requestpool t1,"
				+ "triprequestmap t2,otpstore t3 where t1.userid=? and t3.notification=0 and trim(t3.action)=? and "
				+ "t1.rqeuestid=t2.requestpoolid and t2.tripid=t3.tripid "
				+ "and t2.requestpoolid=t3.requestpoolid";
		ResultSet rs = null;
		
		try{
			conn = DB.getConnection();
			pstmt = conn.prepareStatement(notification);
			pstmt.setInt(1, userid);
			pstmt.setString(2, type);
			rs=pstmt.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("tripid", rs.getString("tripid"));
				obj.put("timestart", rs.getString("timestart"));
				obj.put("otp", rs.getString("otp"));
				jsonArray.put(obj);
				String updateQry="update otpstore set notification=1 where otpid="+rs.getInt("otpid");
				PreparedStatement pst=conn.prepareStatement(updateQry);
				pst.executeUpdate();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return Response.status(200).entity(jsonArray.toString()).build();

	}
	
	
}
