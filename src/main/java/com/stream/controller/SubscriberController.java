package com.stream.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.stream.common.Utility;
import com.stream.facade.IRouteFacade;
import com.stream.facade.RouteFacade;
import com.stream.vo.RouteVO;

@Path("/subscribe")
public class SubscriberController {
	
IRouteFacade routeFacade = new RouteFacade();
	
	@POST
	@Consumes ("application/json")
	@Produces("application/json")
    @Path("/showMatchedRoutesToday")
	public Response showMatchedRoutesToday(RouteVO routesToday){

		JSONObject jsonObject = routeFacade.fetchMyMatchedRoutesToday(routesToday);
		
		if(jsonObject == null) {
			jsonObject = new JSONObject();
			jsonObject.put("operation", "fetchMyPaths");
			jsonObject.put("status", "failure");
		}else{
			jsonObject.put("operation", "fetchMyPaths");
			jsonObject.put("status", "success");
		}
		
		return Response.status(200).entity(jsonObject.toString()).build();		
	}
	
	@GET
	@Produces("application/json")
    @Path("/showPickPointsToday")
	public Response showPointsToday(){

		JSONObject jsonObject = routeFacade.fetchPointsToday();
		
		if(jsonObject == null) {
			jsonObject = new JSONObject();
			jsonObject.put("operation", "fetchPointsToday");
			jsonObject.put("status", "failure");
		}else{
			jsonObject.put("operation", "fetchPointsToday");
			jsonObject.put("status", "success");
		}
		
		return Response.status(200).entity(jsonObject.toString()).build();		
	}

	@POST
	@Consumes ("application/json")
	@Produces("application/json")
    @Path("/bookTheRoute")
	public Response bookTheReoute(RouteVO routesToday){

		JSONObject jsonObject = null;
		try{
			String otp = Utility.otpGenerator();
			jsonObject = routeFacade.bookThisRouteForMe(routesToday,otp);
		
			if(jsonObject == null) {
				jsonObject = new JSONObject();
				jsonObject.put("operation", "bookTheReoute");
				jsonObject.put("status", "failure");
			}else{
				jsonObject.put("operation", "bookTheReoute");
				jsonObject.put("status", "success");
				jsonObject.put("otp", otp);
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
				
		return Response.status(200).entity(jsonObject.toString()).build();		
	}

}

