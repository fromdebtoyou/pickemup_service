package com.stream.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.stream.controller.PropertiesLoader;


/**
 * Opens the DB Connection and keeps it open,
 * Executes Sql and returns stmts
 * prints out results of stmts
 */
public class DB {

	private DB() {}
	
	public static Connection getConnection() throws Exception
	{
		try
		{
			String connectionURL = "jdbc:mysql://localhost:3306/pickemup";
			Connection connection = null;
			Class.forName("com.mysql.jdbc.Driver");
			//connection = DriverManager.getConnection(connectionURL, "root", "root1234");
			connection = DriverManager.getConnection(PropertiesLoader.getProperties().getProperty("database.url"),PropertiesLoader.getProperties().getProperty("database.userid"),
					PropertiesLoader.getProperties().getProperty("database.password"));
			return connection;
		} catch (Exception e)
		{
			throw e;
		}

	}
	

} // DB class
