package com.stream.common;

import java.util.Collection;

public class ResponseTypes {
	
	public static final String USER_REGISTERED = "The User successfully registered in the system";
	public static final String USER_REGISTERED_MAIL_SENT = "The User successfully registered and invitation mail sent";
	public static final String USER_ALREADY_EXISTS = "The User already registered in the system"; 
	public static final String USER_SIGN_UP_COMPLETE = "The User successfully signed-up in the system";
	public static final String USER_NOT_REGISTERED = "The User information absent";
	public static final String PASSWORD_ERROR = "The password provided is wrong";
	public static final String MAC_ID_MISMATCH = "The mac id is not same as registered. Please contact administrator";
	public static final String MAIL_ACTIVATION = "Mail.Activation";
	public static final String USER_LOGIN_WITH_NEW_MACID = "The User logged in with different mac id";
	

}
