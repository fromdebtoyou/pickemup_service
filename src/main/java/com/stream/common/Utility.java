package com.stream.common;

import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.stream.db.DB;

public class Utility {
	
	static final String FROM = "catalog@vishnustrumpet.com";   // Replace with your "From" address. This address must be verified.
    static final String TO = "oolta@gmail.com";  // Replace with a "To" address. If your account is still in the 
                                                       // sandbox, this address must be verified.
    
    public static final String BODY = "This email was sent through the Amazon SES SMTP interface by using Java.";
    public static final String BODY_NEW_USER = "<html>Dear @@user@@<br>You are cordially invited to join our exclusive \"find your jams\" new music discovery club.<br>"+
    		"Here's your user name, password and link to our mobile app:<br>user name: @@email@@<br>password:@@password@@<br>"+
    		"iPhone app download link:<a href='http://google.com'>Download<a><br>Android app download link:<a href='http://google.com'>Download<a>"+
    		"<hr>As part of the \"find your jams\" club, you will receive a new album every week from a different amazing indie artist we think you<br>"+
    		"will love. Each album is delivered to our simple, intuitive mobile app that you can listen to anywhere, anytime. There's no telecom<br>"+
    		"connection needed to play the music, which means no extra data fees for you and no buffering.<br>When you find a jam you love and can't "+
    		"live without, add it to a custom playlist that's there for you whenever you need it.<br>You have all week to dig in to the artist and "+
    		"album. Instead of ephemeral services where the music comes and goes quickly, <br>"+
    		"we encourage you to sit back and give the music a good listen. Pour yourself a drink, get together with friends, compare notes<br>"+
    		"on which part of the album you enjoy best.<br><br>Best of all, as a subscriber you get to own and control the music. Like an album? "+
    		"You can download it to your computer, share it<br>with friends, write a cover version of one of the songs. There's never any DRM or "+
    		"other nonsense. It's free for you to use as you <br>like for non-commercial purposes.<br><br>We are setting the music free. The way it "+ 
    		"should be.<hr>Try a 14-day free trial of our exclusive \"find your jams\" music discovery club.<br>We're a group of music lovers like "+
    		"yourself who love discovering new music, but who hate the way existing services are structured to<br>systematically exploit hard-working "+ 
    		"artists. Actually it seems like most of the world works that way, doesn't it?<br><br>It's time for something better.<br>"+
    		"<br>Be part of a creative renaissance that will shift the global zeitgeist towards harmony and peace among all beings.<br><br>Join us.<br>"+
    		"<br>with love,<br>the vt team</html>";
    public static final String BODY_PIN_RESET = "<html>Dear @@user@@<br>Your password is reset.<br>"+
    		"Here's your user name, password and link to our mobile app:<br>user name: @@email@@<br>password:@@password@@<br>"+
    		"iPhone app download link:<a href='http://google.com'>Download<a><br>Android app download link:<a href='http://google.com'>Download<a>"+
    		"<hr>As part of the \"find your jams\" club, you will receive a new album every week from a different amazing indie artist we think you<br>"+
    		"will love. Each album is delivered to our simple, intuitive mobile app that you can listen to anywhere, anytime. There's no telecom<br>"+
    		"connection needed to play the music, which means no extra data fees for you and no buffering.<br>When you find a jam you love and can't "+
    		"live without, add it to a custom playlist that's there for you whenever you need it.<br>You have all week to dig in to the artist and "+
    		"album. Instead of ephemeral services where the music comes and goes quickly, <br>"+
    		"we encourage you to sit back and give the music a good listen. Pour yourself a drink, get together with friends, compare notes<br>"+
    		"on which part of the album you enjoy best.<br><br>Best of all, as a subscriber you get to own and control the music. Like an album? "+
    		"You can download it to your computer, share it<br>with friends, write a cover version of one of the songs. There's never any DRM or "+
    		"other nonsense. It's free for you to use as you <br>like for non-commercial purposes.<br><br>We are setting the music free. The way it "+ 
    		"should be.<hr>Try a 14-day free trial of our exclusive \"find your jams\" music discovery club.<br>We're a group of music lovers like "+
    		"yourself who love discovering new music, but who hate the way existing services are structured to<br>systematically exploit hard-working "+ 
    		"artists. Actually it seems like most of the world works that way, doesn't it?<br><br>It's time for something better.<br>"+
    		"<br>Be part of a creative renaissance that will shift the global zeitgeist towards harmony and peace among all beings.<br><br>Join us.<br>"+
    		"<br>with love,<br>the vt team</html>";
    static final String SUBJECT = "VT Login information";
    public static final String SUBJECT_NEW_USER = "@@ReferrerName@@ refers you to join our exclusive new music discovery club.";
    public static final String SUBJECT_NEW_USER_NoREF = "Join our exclusive new music discovery club.";
    public static final String SUBJECT_PIN_REST = "VT Login : Pin reset";
    
    // Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.
    static final String SMTP_USERNAME = "AKIAIWS2UKOUYD5GILGQ";//"AKIAIFPZSKXDNWC6QXLA";//AKIAIWS2UKOUYD5GILGQ  // Replace with your SMTP username.
    static final String SMTP_PASSWORD = "AvouQ5D7AFQ6cqthQNSKQmGcAi1zaRmvPpYb2e944Zzn";//"Au0qjzullg/SBfeee0nCime3J+zhBDG4yup/qFlV2M8U";//AvouQ5D7AFQ6cqthQNSKQmGcAi1zaRmvPpYb2e944Zzn  // Replace with your SMTP password.
    
    // Amazon SES SMTP host name. This example uses the US West (Oregon) region.
    static final String HOST = "email-smtp.us-west-2.amazonaws.com";    
    
    // Port we will connect to on the Amazon SES SMTP endpoint. We are choosing port 25 because we will use
    // STARTTLS to encrypt the connection.
    static final int PORT = 2587;
	
	public static String generatePIN() 
	{   
	   /* int x = (int)(Math.random() * 9);
	    x = x + 1;
	    String randomPIN = (x + "") + ( ((int)(Math.random()*1000)) + "" );*/
		int pin = (int)(Math.random()*9000)+1000;
		String randomPIN = "" + pin;
	    return randomPIN;
	}
	
	/*public static boolean sendMail(String email, String name, String Password){
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		
		Session session = null;

		try {

		session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("invitation.vtapps","Kolkata@123");
				}
			});

		
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("invitation.vtapps@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(email));
			message.setSubject("VT APP INVITATION");
			message.setText("Dear " +name +","+
					"\n\n Your password is: " + Password);

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			//throw new RuntimeException(e);
			e.printStackTrace();
			emailSendingFailedLog(email,"registration");
		} catch (Exception e) {
			//throw new RuntimeException(e);
			e.printStackTrace();
			emailSendingFailedLog(email,"registration");
			return false;
		}
		return true;
	}
	
	public static boolean sendMail(String email, String name, String Password){
		// Create a Properties object to contain connection configuration information.
    	Properties props = System.getProperties();
    	props.put("mail.transport.protocol", "smtp");
    	props.put("mail.smtp.port", PORT); 
    	
    	// Set properties indicating that we want to use STARTTLS to encrypt the connection.
    	// The SMTP session will begin on an unencrypted connection, and then the client
        // will issue a STARTTLS command to upgrade to an encrypted connection.
    	props.put("mail.smtp.auth", "true");
    	props.put("mail.smtp.starttls.enable", "true");
    	props.put("mail.smtp.starttls.required", "true");

        // Create a Session object to represent a mail session with the specified properties. 
    	Session session = Session.getDefaultInstance(props);
    	Transport transport = null;
        // Create a message with the specified information. 
        MimeMessage msg = new MimeMessage(session);
        try {
			msg.setFrom(new InternetAddress(FROM));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
		    msg.setSubject(SUBJECT);
		    msg.setContent(BODY,"text/html");
		 // Create a transport.        
	        transport = session.getTransport();
	        System.out.println("Attempting to send an email through the Amazon SES SMTP interface...");
            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST,2587,SMTP_USERNAME, SMTP_PASSWORD);
        	// Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Email sent!");
        }catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			emailSendingFailedLog(email,"registration");
		}catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
            ex.printStackTrace();
            emailSendingFailedLog(email,"registration");
        }
        finally
        {
            // Close and terminate the connection.
            try {
				transport.close();
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}        	
        }
        return true;
	}
	
	public static boolean sendMail(String email, String name, String Password,String subject,String body){
		// Create a Properties object to contain connection configuration information.
    	Properties props = System.getProperties();
    	props.put("mail.transport.protocol", "smtp");
    	props.put("mail.smtp.port", PORT); 
    	
    	// Set properties indicating that we want to use STARTTLS to encrypt the connection.
    	// The SMTP session will begin on an unencrypted connection, and then the client
        // will issue a STARTTLS command to upgrade to an encrypted connection.
    	props.put("mail.smtp.auth", "true");
    	props.put("mail.smtp.starttls.enable", "true");
    	props.put("mail.smtp.starttls.required", "true");

        // Create a Session object to represent a mail session with the specified properties. 
    	Session session = Session.getDefaultInstance(props);
    	Transport transport = null;
        // Create a message with the specified information. 
        MimeMessage msg = new MimeMessage(session);
        try {
			msg.setFrom(new InternetAddress(FROM));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
		    msg.setSubject(subject);
		    msg.setContent(body,"text/html");
		 // Create a transport.        
	        transport = session.getTransport();
	        System.out.println("Attempting to send an email through the Amazon SES SMTP interface...");
            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST,2587,SMTP_USERNAME, SMTP_PASSWORD);
        	// Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Email sent!");
        }catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			emailSendingFailedLog(email,"registration");
		}catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
            ex.printStackTrace();
            emailSendingFailedLog(email,"registration");
        }
        finally
        {
            // Close and terminate the connection.
            try {
				transport.close();
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}        	
        }
        return true;
	}
	
	private static void emailSendingFailedLog(String email,String stage) {
    	Connection con = null;
    	PreparedStatement preparedStatement = null;
		try{
			con = DB.getConnection();
			
			String insertTableSQL = "INSERT INTO mailfailurelog (email,failuretime,stage) VALUES(?,NOW(),?)";
			preparedStatement = con.prepareStatement(insertTableSQL);

			preparedStatement.setString(1, email);
			preparedStatement.setString(2, stage);
			preparedStatement.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				preparedStatement.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
*/
	private static String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return stringBuffer.toString();
    }
	public static String hashString(String message)  {
	 
	    try {
	        MessageDigest digest = MessageDigest.getInstance("MD5");
	        byte[] hashedBytes = digest.digest(message.getBytes("UTF-8"));
	 
	        return convertByteArrayToHexString(hashedBytes);
	    } catch (Exception ex) {
	    		ex.printStackTrace();

	    }
	    return null;
	}
	
	public static String otpGenerator(){
		String otp = "";
		
		otp = generatePIN();
		System.out.println(otp);
		
		return otp;
	}
	
	public static void main(String args[]){
		otpGenerator();	
	}
	
}
