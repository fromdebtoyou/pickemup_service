package com.stream.exception;

public class StreamCustomException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String LOGIN_FAILURE_CODE = "40";
	public static final String LOGIN_FAILURE_MESSAGE = "Login Failed: Wrong email/password";
	public static final String LOGIN_FAILURE_WRONG_USERNAME_PASSWORD_CODE = "41";
	public static final String LOGIN_FAILURE_WRONG_USERNAME_PASSWORD_MESSAGE = "Login Failed: Wrong email/password";
	public static final String LOGIN_FAILURE_WRONG_MACID_CODE = "42";
	public static final String LOGIN_FAILURE_WRONG_MACID_MESSAGE = "Mac Id does not match";
	public static final String USER_NOT_REGISTERED_CODE = "43";
	public static final String USER_NOT_REGISTERED_MESSAGE = "User not registerd";
	public static final String PLAYLIST_UPDATE_FAILED_CODE = "67";
	public static final String PLAYLIST_UPDATE_FAILED_MESSAGE = "Playlist update failed";
	public static final String PASSWORDS_DO_NOT_MATCH_CODE = "93";
	public static final String PASSWORDS_DO_NOT_MATCH_MESSAGE = "The passwords do not match";
	public static final String LOGIN_FROM_DEVICE_FIRST_CODE = "96";
	public static final String LOGIN_FROM_DEVICE_FIRST_MESSAGE = "The web login is not enabled currently. Please login from your Android/iOS device first to activate web login";
	
	
	
	private int messageCode;
	private String errorMessage;
	
	public StreamCustomException() {
		
	}
	
	public StreamCustomException(String errCode,String message) {
		// TODO Auto-generated constructor stub
		this.setMessageCode(messageCode);
		this.setErrorMessage(message);
	}

	public int getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(int messageCode) {
		this.messageCode = messageCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	

}
