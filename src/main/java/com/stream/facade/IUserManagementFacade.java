package com.stream.facade;

import org.json.JSONObject;

import com.stream.exception.StreamCustomException;
import com.stream.vo.LoginVO;
import com.stream.vo.RegistrationVO;
import com.stream.vo.UserVO;

public interface IUserManagementFacade {
	public JSONObject login(UserVO userObj);
	
}
