package com.stream.facade;

import org.json.JSONObject;

import com.stream.vo.PickUpPointsVO;
import com.stream.vo.RouteVO;
import com.stream.vo.TripVO;

public interface IRouteFacade {
	public JSONObject createPath(RouteVO route);

	public void createPickupPoints(PickUpPointsVO[] pickupPoints, int routeid, String string, String string2);

	public JSONObject fetchMyPaths(int userid);

	public JSONObject showMeARoute(int routeid);

	public JSONObject fetchMyMatchedRoutesToday(RouteVO routesToday);

	public JSONObject fetchPointsToday();

	public JSONObject bookThisRouteForMe(RouteVO routesToday, String otp) throws Exception;

	public JSONObject getSubscribers(int routeid);

	public JSONObject getSubscriber(int subid);

	public JSONObject createTrip(TripVO trip) throws Exception;
}
