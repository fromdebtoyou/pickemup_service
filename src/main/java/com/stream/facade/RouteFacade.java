package com.stream.facade;

import java.util.List;

import org.json.JSONObject;

import com.stream.dao.IRouteDAO;
import com.stream.dao.RouteDAO;
import com.stream.vo.PickUpPointsVO;
import com.stream.vo.RouteVO;
import com.stream.vo.SubscriberVO;
import com.stream.vo.TripVO;
import com.stream.vo.UserVO;

public class RouteFacade implements IRouteFacade{
	
	public static IRouteDAO routeDAO = new RouteDAO();

	public JSONObject createPath(RouteVO route) {
		JSONObject jsonObj = null;
		RouteVO routeVO = routeDAO.createRoute(route);
		
		if(routeVO != null){
			jsonObj = new JSONObject(routeVO);
		}
		
		return jsonObj;
	}

	public void createPickupPoints(PickUpPointsVO[] pickupPoints,int routeid,String origin,String destination) {
		routeDAO.createPickupPoints(pickupPoints,routeid,origin,destination);
	}

	public JSONObject fetchMyPaths(int userid) {
		// TODO Auto-generated method stub
		return routeDAO.fetchMyPaths(userid);
	}

	public JSONObject showMeARoute(int routeid) {
		// TODO Auto-generated method stub
		return routeDAO.showMeARoute(routeid);
	}

	public JSONObject fetchMyMatchedRoutesToday(RouteVO routesToday) {
		return routeDAO.fetchMyMatchedRoutesToday(routesToday);
	}

	public JSONObject fetchPointsToday() {
		return routeDAO.fetchPointsToday();
	}

	public JSONObject bookThisRouteForMe(RouteVO routesToday,String otp) throws Exception{
		// TODO Auto-generated method stub
		routeDAO.bookThisRouteForMe(routesToday,otp);
		return new JSONObject();
	}

	public JSONObject getSubscribers(int routeid) {
		// TODO Auto-generated method stub
		List<SubscriberVO> subsList = routeDAO.getSubscribers(routeid);
		
		JSONObject subscribersJSON = new JSONObject();
		subscribersJSON.put("subscribers",subsList.toArray());
		
		return subscribersJSON;
	}

	public JSONObject getSubscriber(int subid) {
		// TODO Auto-generated method stub
		SubscriberVO subscriber = routeDAO.getSubscriber(subid);
		JSONObject subscribersJSON = new JSONObject(subscriber);
		//subscribersJSON.put("subscriberdetails",subscriber);
		return subscribersJSON;
	}

	public JSONObject createTrip(TripVO trip) throws Exception {
		routeDAO.createTrip(trip);
		return new JSONObject();
	}

}
