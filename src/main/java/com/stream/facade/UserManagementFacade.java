package com.stream.facade;

import java.util.List;

import org.json.JSONObject;

import com.stream.common.ResponseTypes;
import com.stream.common.Utility;
import com.stream.dao.IUserManagementDAO;
import com.stream.dao.UserManagementDAO;
import com.stream.exception.StreamCustomException;
import com.stream.vo.LoginVO;
import com.stream.vo.RegistrationVO;
import com.stream.vo.UserVO;

public class UserManagementFacade implements IUserManagementFacade{

	public static IUserManagementDAO userManagementDAO = new UserManagementDAO();
	
	
	public JSONObject login(UserVO userObj) {
		
		JSONObject jsonObject = null;
		UserVO userDBObject = null;
		try{
		
			userDBObject = userManagementDAO.authenticate(userObj); //Do not match mac id
			
			if(userDBObject != null){
				jsonObject = new JSONObject(userDBObject);
			}
			
				
		}catch(Exception ex){
			jsonObject = new JSONObject(ex);
			ex.printStackTrace();
		}
		return jsonObject;
	}
		
		
}
