CREATE DATABASE  IF NOT EXISTS `pickemup` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pickemup`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: pickemup
-- ------------------------------------------------------
-- Server version	5.5.46

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `feedbackid` int(11) NOT NULL,
  `feedback` varchar(450) DEFAULT NULL,
  `rating` int(11) NOT NULL,
  `journeyid` int(11) NOT NULL,
  `assubscriber` int(11) NOT NULL,
  `asprovider` int(11) NOT NULL,
  PRIMARY KEY (`feedbackid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otpstore`
--

DROP TABLE IF EXISTS `otpstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otpstore` (
  `otpid` int(11) NOT NULL AUTO_INCREMENT,
  `otp` varchar(45) DEFAULT NULL,
  `requestpoolid` int(11) DEFAULT NULL,
  `tripid` int(11) DEFAULT NULL,
  `action` varchar(45) DEFAULT NULL,
  `createdon` timestamp NULL DEFAULT NULL,
  `authenticated` int(11) DEFAULT NULL,
  `authenticatedon` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`otpid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otpstore`
--

LOCK TABLES `otpstore` WRITE;
/*!40000 ALTER TABLE `otpstore` DISABLE KEYS */;
INSERT INTO `otpstore` VALUES (1,'6304',7,4,'root_booking','2017-11-04 10:54:25',0,NULL),(2,'7240',11,3,' Route Set','2018-01-14 06:07:17',NULL,NULL),(3,'8332',10,3,' Route Set','2018-01-14 06:07:17',NULL,NULL),(4,'5432',7,3,' Route Set','2018-01-14 06:07:17',NULL,NULL),(5,'4080',10,3,' Completion of Journey','2018-01-27 09:56:31',NULL,NULL);
/*!40000 ALTER TABLE `otpstore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requestdensity`
--

DROP TABLE IF EXISTS `requestdensity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requestdensity` (
  `routeid` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `starttime` varchar(45) DEFAULT NULL,
  `endtime` varchar(45) DEFAULT NULL,
  `startseq` int(11) DEFAULT NULL,
  `endseq` int(11) DEFAULT NULL,
  `requestpoolid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requestdensity`
--

LOCK TABLES `requestdensity` WRITE;
/*!40000 ALTER TABLE `requestdensity` DISABLE KEYS */;
INSERT INTO `requestdensity` VALUES (6,1,'10:00','10:59',NULL,NULL,3),(7,1,'10:00','10:59',NULL,NULL,NULL),(9,1,'10:00','10:59',NULL,NULL,NULL),(6,1,'10:00','10:59',NULL,NULL,3),(7,1,'10:00','10:59',NULL,NULL,NULL),(9,1,'10:00','10:59',NULL,NULL,NULL),(6,1,'10:00','10:59',NULL,NULL,3),(7,1,'10:00','10:59',NULL,NULL,NULL),(9,1,'10:00','10:59',NULL,NULL,NULL),(6,1,'12:00','12:59',1,15,3),(7,1,'12:00','12:59',1,17,NULL),(9,1,'12:00','12:59',1,17,NULL),(6,1,'2:00','2:59',1,15,11),(7,1,'2:00','2:59',1,17,11),(9,1,'2:00','2:59',1,17,11);
/*!40000 ALTER TABLE `requestdensity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requestpool`
--

DROP TABLE IF EXISTS `requestpool`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requestpool` (
  `rqeuestid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(45) DEFAULT NULL,
  `startpointid` varchar(45) DEFAULT NULL,
  `endpointid` varchar(45) DEFAULT NULL,
  `timestart` varchar(45) DEFAULT NULL,
  `createdon` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `isbooked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rqeuestid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requestpool`
--

LOCK TABLES `requestpool` WRITE;
/*!40000 ALTER TABLE `requestpool` DISABLE KEYS */;
INSERT INTO `requestpool` VALUES (3,'3','47','18','10:30','2017-12-25 11:14:34',0),(5,'3','47','18','10:30','2017-12-25 11:16:03',0),(6,'3','47','18','10:30','2017-12-25 11:17:07',0),(7,'3','47','18','12:30','2017-12-30 11:13:02',0),(10,'3','47','18','12:30','2017-12-30 11:22:04',0),(11,'3','47','18','2:30','2018-01-07 14:58:51',0);
/*!40000 ALTER TABLE `requestpool` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routelinks`
--

DROP TABLE IF EXISTS `routelinks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routelinks` (
  `RLId` int(11) NOT NULL AUTO_INCREMENT,
  `RouteId` int(11) DEFAULT NULL,
  `RoutePointId` int(11) DEFAULT NULL,
  `PrevPointId` int(11) DEFAULT NULL,
  `PrevIdDist` double DEFAULT NULL,
  `Sequence` int(11) DEFAULT NULL,
  PRIMARY KEY (`RLId`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routelinks`
--

LOCK TABLES `routelinks` WRITE;
/*!40000 ALTER TABLE `routelinks` DISABLE KEYS */;
INSERT INTO `routelinks` VALUES (1,1,1,0,0,1),(2,1,2,1,1.5,2),(3,1,8,2,1,3),(4,1,9,8,1.5,4),(5,1,10,9,1.5,5),(6,1,11,10,1,6),(7,1,14,11,2,7),(8,1,15,14,2,8),(9,1,16,15,1.5,9),(10,1,17,16,1,10),(11,1,18,17,1,11),(13,17,17,18,1,2),(14,17,16,17,1,3),(15,17,15,16,1.5,4),(16,17,14,15,2,5),(17,17,11,14,2,6),(18,17,10,11,1,7),(19,17,9,10,1.5,8),(20,17,8,9,1.5,9),(21,17,2,8,1,10),(22,17,1,2,1.5,11),(23,9,47,0,0,1),(24,9,46,47,1,2),(25,9,45,46,1,3),(26,9,44,45,1,4),(27,9,71,44,1,5),(28,9,70,71,1,6),(29,9,69,70,1,7),(30,9,29,69,1,8),(31,9,30,29,1,9),(32,9,28,30,1,10),(33,9,27,28,1,11),(34,9,26,27,1,12),(35,9,22,26,1,13),(36,9,21,22,1,14),(37,9,20,21,1,15),(38,9,19,20,1,16),(39,9,18,19,1,17),(40,9,17,18,1,18),(41,9,16,17,1,19),(42,9,15,16,1,20),(43,25,15,0,0,1),(44,25,16,15,1,2),(45,25,17,16,1,3),(46,25,18,17,1,4),(47,25,19,18,1,5),(48,25,20,19,1,6),(49,25,21,20,1,7),(50,25,22,21,1,8),(51,25,26,22,1,9),(52,25,27,26,1,10),(53,25,28,27,1,11),(54,25,30,28,1,12),(55,25,29,30,1,13),(56,25,69,29,1,14),(57,25,70,69,1,15),(58,25,71,70,1,16),(59,25,44,71,1,17),(60,25,45,44,1,18),(61,25,46,45,1,19),(62,25,47,46,1,20),(63,6,47,0,0,1),(64,6,46,47,1.5,2),(65,6,45,46,1,3),(66,6,54,45,1.5,4),(67,6,55,54,1.5,5),(68,6,57,55,1,6),(69,6,58,57,2,7),(70,6,59,58,2,8),(71,6,60,59,1.5,9),(72,6,61,60,1,10),(73,6,22,61,1,11),(75,6,20,21,1,13),(76,6,19,20,1,14),(77,6,18,19,1.5,15),(78,22,18,0,0,1),(79,22,19,18,2,2),(80,22,20,19,1,3),(81,22,21,20,1.5,4),(82,22,22,21,1.5,5),(83,22,61,22,1,6),(84,22,60,61,1.5,7),(85,22,59,60,0,8),(86,22,58,59,1,9),(87,22,57,58,1,10),(88,22,55,57,1,11),(89,22,54,55,1,12),(90,22,45,54,1,13),(91,22,46,45,1,14),(92,22,47,46,1,15),(93,7,47,0,0,1),(94,7,46,47,1.5,2),(95,7,45,46,1,3),(96,7,54,45,1.5,4),(97,7,55,54,1.5,5),(98,7,57,55,1,6),(99,7,58,57,2,7),(100,7,59,58,2,8),(101,7,63,59,1,9),(102,7,64,63,1,10),(103,7,65,64,1,11),(104,7,62,65,1,12),(105,7,22,62,1,13),(106,7,21,22,1,14),(107,7,20,21,1,15),(108,7,19,20,1,16),(109,7,18,19,1.5,17),(110,23,18,0,0,1),(111,23,19,18,2,2),(112,23,20,19,1,3),(113,23,21,20,1.5,4),(114,23,22,21,1.5,5),(115,23,63,64,1,9),(117,23,59,63,1,10),(118,23,58,59,1,11),(119,23,57,58,1,12),(120,23,55,57,1,13),(121,23,54,55,1,14),(122,23,45,54,1,15),(123,23,46,45,1,16),(124,23,47,46,1,17);
/*!40000 ALTER TABLE `routelinks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routepoints`
--

DROP TABLE IF EXISTS `routepoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routepoints` (
  `RoutePointId` int(11) NOT NULL AUTO_INCREMENT,
  `Description` text,
  `Latitude` text,
  `Longitude` text,
  `isFlyover` text,
  PRIMARY KEY (`RoutePointId`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routepoints`
--

LOCK TABLES `routepoints` WRITE;
/*!40000 ALTER TABLE `routepoints` DISABLE KEYS */;
INSERT INTO `routepoints` VALUES (1,'Airport','','',''),(2,'Kaikhali','','',''),(3,'Baguiati','','',''),(4,'Kestopur','','',''),(5,'Lake Town','','',''),(6,'Hudco More','','',''),(7,'Bidhan Nagar Rly Stn','','',''),(8,'Haldiram','','',''),(9,'Chinar Park','','',''),(10,'City Center 2','','',''),(11,'Eco Park','','',''),(12,'Alia University','','',''),(13,'DLF 2','','',''),(14,'Narkel Bagan','','',''),(15,'DLF 1','','',''),(16,'Technopolis','','',''),(17,'College More','','',''),(18,'SDF','','',''),(19,'Swastha Bhavan','','',''),(20,'Nicco Park','','',''),(21,'Sukanta Nagar','','',''),(22,'Chingrihata','','',''),(23,'Beleghata','','',''),(24,'Mani Square','','',''),(25,'Swabhumi','','',''),(26,'Metropolitan','','',''),(27,'Science City','','',''),(28,'VIP Bazar','','',''),(29,'Ruby','','',''),(30,'Uttor Panchanna','','',''),(31,'Mukundapur','','',''),(32,'Ananda Nagar','','',''),(33,'Garia','','',''),(34,'Ganguly Bagan','','',''),(35,'Bagha Jatin','','',''),(36,'Jadavpur 8B','','',''),(37,'Jadavpur PS','','',''),(38,'Jodhpur Park','','',''),(39,'Naveena','','',''),(40,'South City','','',''),(41,'Anwar Shah More','','',''),(42,'Tollygunje Phari','','',''),(43,'MahabirTala','','',''),(44,'New Alipore Petrol Pump','','',''),(45,'Taratala','','',''),(46,'Behala Thana','','',''),(47,'Behala Chowrasta','','',''),(48,'Thakurpukur Market','','',''),(49,'Dakghar','','',''),(50,'BanerjeeHat','','',''),(51,'Sibrampur','','',''),(52,'Sakuntala Park','','',''),(53,'Bakultala','','',''),(54,'Mominpur','','',''),(55,'Ekbalpur','','',''),(56,'Khidirpur','','',''),(57,'Alipore Zoo','','',''),(58,'PTS','','',''),(59,'J C Bose Flyover','','','Y'),(60,'Maa Flyover','','','Y'),(61,'Bartaman Office  EM Bypass','','',''),(62,'Parama Island','','',''),(63,'Park Circus Rly Bridge(4 No)','','',''),(64,'Tapsia','','',''),(65,'China Town','','',''),(66,'PC Chandra','','',''),(67,'Leather Complex','','',''),(68,'Wipro Flyover','','','Y'),(69,'Bose Pukur','','',''),(70,'Kasba','','',''),(71,'Gariahat More','','',''),(72,'Rasbehari More','','',''),(73,'Chetla','','',''),(74,'Durgapur Bridge','','','Y');
/*!40000 ALTER TABLE `routepoints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routes`
--

DROP TABLE IF EXISTS `routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routes` (
  `RouteId` int(11) NOT NULL AUTO_INCREMENT,
  `RouteName` varchar(45) DEFAULT NULL,
  `StartPointID` int(11) DEFAULT NULL,
  `EndPointId` int(11) DEFAULT NULL,
  `Direction` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`RouteId`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routes`
--

LOCK TABLES `routes` WRITE;
/*!40000 ALTER TABLE `routes` DISABLE KEYS */;
INSERT INTO `routes` VALUES (1,'Airport - SDF via Chinar Park',1,18,'FWD'),(2,'Airport - DLF 1 via Unitech',1,15,'FWD'),(3,'Airport - DLF 1 Narkel Bagan',1,15,'FWD'),(4,'Airport - SDF via Bidhan Nagar',1,18,'FWD'),(5,'Airport - DLF1 via Bidhan Nagar',1,15,'FWD'),(6,'Behala - SDF via J C Bose & Maa Flyover',47,18,'FWD'),(7,'Behala - SDF via Park Circus Rly Bridge(4 No)',47,18,'FWD'),(8,'Behala - SDF vi Taratala & Ruby',47,18,'FWD'),(9,'Behala - DLF 1 via SDF & Ruby',47,15,'FWD'),(10,'Behala - DLF 2 via SDF',47,13,'FWD'),(11,'Behala - DLF 1 via Wipro Flyover',47,15,'FWD'),(12,'Behala - DLF 2 via Wipro Flyover',47,13,'FWD'),(13,'Garia - DLF 1 via SDF',33,15,'FWD'),(14,'Garia - DLF 2 via SDF',33,13,'FWD'),(15,'Garia - DLF 1 via Wipro Flyover',33,15,'FWD'),(16,'Garia - DLF 2 via Wipro Flyover',33,13,'FWD'),(17,'SDF - Airport via Chinar Park',18,1,'REV'),(18,'DLF 1  - Airport via Unitech',15,1,'REV'),(19,'DLF 1 - Airport via Narkel Bagan',15,1,'REV'),(20,'SDF - Airport via Bidhan Nagar',18,1,'REV'),(21,'DLF1 - Airport via Bidhan Nagar',15,1,'REV'),(22,'SDF - Behala via J C Bose & Maa Flyover',18,47,'REV'),(23,'SDF - Behala via Park Circus Rly Bridge(4 No)',18,47,'REV'),(24,'SDF - Behala via Taratala & Ruby',18,47,'REV'),(25,'DLF 1 - Behala via SDF & Ruby',15,47,'REV'),(26,'DLF 2 - Behala via SDF',13,47,'REV'),(27,'DLF 1  - Behala via Wipro Flyover',15,47,'REV'),(28,'DLF 2 - Behala via Wipro Flyover',13,47,'REV'),(29,'DLF 1 - Garia  via SDF',15,33,'REV'),(30,'DLF 2 - Garia  via SDF',13,33,'REV'),(31,'DLF 1 - Garia via Wipro Flyover',15,33,'REV'),(32,'DLF 2 - Garia via Wipro Flyover',13,33,'REV');
/*!40000 ALTER TABLE `routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `triprequestmap`
--

DROP TABLE IF EXISTS `triprequestmap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `triprequestmap` (
  `tripid` int(11) NOT NULL,
  `requestpoolid` int(11) NOT NULL,
  `lastrecordedlat` varchar(45) DEFAULT NULL,
  `lastrecordedlong` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`tripid`,`requestpoolid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `triprequestmap`
--

LOCK TABLES `triprequestmap` WRITE;
/*!40000 ALTER TABLE `triprequestmap` DISABLE KEYS */;
INSERT INTO `triprequestmap` VALUES (1,1,NULL,NULL,1),(1,3,NULL,NULL,1),(1,4,NULL,NULL,1),(2,9,NULL,NULL,0),(2,11,NULL,NULL,0),(2,13,NULL,NULL,0),(3,7,NULL,NULL,0),(3,10,NULL,NULL,0),(3,11,NULL,NULL,0);
/*!40000 ALTER TABLE `triprequestmap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tripstore`
--

DROP TABLE IF EXISTS `tripstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tripstore` (
  `tripid` int(11) NOT NULL AUTO_INCREMENT,
  `routeid` int(11) DEFAULT NULL,
  `createdon` timestamp NULL DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `providedby` varchar(45) DEFAULT NULL,
  `tripstarttime` timestamp NULL DEFAULT NULL,
  `tripstartpoint` int(11) DEFAULT NULL,
  `tripendpoint` int(11) DEFAULT NULL,
  `carmake` varchar(45) DEFAULT NULL,
  `carmodel` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tripid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tripstore`
--

LOCK TABLES `tripstore` WRITE;
/*!40000 ALTER TABLE `tripstore` DISABLE KEYS */;
INSERT INTO `tripstore` VALUES (1,17,'2017-11-11 05:01:35','1',NULL,NULL,NULL,NULL,NULL,NULL),(2,3,'2018-01-14 05:58:54','1','3','2018-01-15 05:45:51',47,18,'Maruti','Alto'),(3,3,'2018-01-14 06:06:01','0','2','2018-01-15 05:45:51',47,18,'Maruti','Alto');
/*!40000 ALTER TABLE `tripstore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `mobileno` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `deviceid` varchar(45) DEFAULT NULL,
  `imageurl` varchar(200) DEFAULT NULL,
  `govtidtype` varchar(45) DEFAULT NULL,
  `goveidnumber` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'brucewen','8697730428','pass','fjd73659','/PickemupService/images/girlface.png',NULL,NULL),(2,'clarkkent','9836212500','pass','hjg783473','/PickemupService/images/girlface.png',NULL,NULL),(3,'popeye','8697730428','pass','kkdfd653463','/PickemupService/images/sadface.png',NULL,NULL),(4,'tonystarc','9836212500','pass','ute784ghfdj','/PickemupService/images/spceface.png',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'pickemup'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-27 15:46:40
